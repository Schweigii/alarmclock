/*
 * Created by JFormDesigner on Mon Apr 06 17:46:56 CEST 2020
 */

package alarmclock.gui;

import alarmclock.gui.choosealarm.ChooseAlarmDialog;
import alarmclock.gui.generalguicomponents.labels.datetime.DateLabel;
import alarmclock.gui.generalguicomponents.labels.datetime.TimeLabel;
import alarmclock.gui.generalguicomponents.panels.GUIAction;
import alarmclock.gui.generalguicomponents.panels.OkAndCancelBtnPanel;
import alarmclock.gui.showalarms.ShowAlarmsDialog;
import alarmclock.logiclayer.manager.alarmmanager.AlarmManager;
import alarmclock.logiclayer.manager.obersvers.AlarmObserver;
import alarmclock.logiclayer.manager.voicerecognition.exceptions.InvalidAlarmDateTimeException;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.time.format.DateTimeFormatter;

/**
 * @author Manuel Schweighofer
 * The main frame that shows the current time and date as well as the next alarm
 */
public class Clock extends JFrame implements AlarmObserver {
  /**
   * The {@link AlarmManager} instance for setting new alarms and showing the current ones
   */
  private AlarmManager alarmManager = AlarmManager.getInstance();

  /**
   * This Constructor initializes the GUI-Components and calls the custom firstInit afterwards
   */
  public Clock() {
    initComponents();
    firstInit();
  }

  /**
   * The custom init method that does a bunch of stuff like setting size and location or customising the buttonPanel and starting the AlarmTask
   */
  private void firstInit() {
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    setSize(screenSize);
    setLocation(0, 0);
    alarmManager.addObserver(this);
    buttonPanel.addCancelActionListener(e -> System.exit(0));
    buttonPanel.hideOkButton();
    update();
    new AlarmTask();
  }

  /**
   * The action event for the setAlarmBtn
   * Creates a new Dialog and uses the result of this dialog to add a new alarm via the alarm manager
   *
   * @see ChooseAlarmDialog
   */
  private void setAlarmBtnActionPerformed() {
    ChooseAlarmDialog chooseAlarmDialog = new ChooseAlarmDialog(this);
    chooseAlarmDialog.showDialog();
    if (chooseAlarmDialog.getAction() == GUIAction.OK) {
      try {
        alarmManager.setNewAlarmTime(chooseAlarmDialog.getSetAlarmDateTime());
      } catch (InvalidAlarmDateTimeException e) {
        JOptionPane.showMessageDialog(SwingUtilities.getWindowAncestor(this), e.getMessage(), e.getClass().getSimpleName(), JOptionPane.ERROR_MESSAGE);
        System.out.println(e.getMessage());
      }
    }
  }

  /**
   * updates the <code>alarmDate</code> and <code>alarmTime</code> when an alarm has changed
   */
  @Override
  public void update() {
    alarmManager.getNextAlarm().ifPresentOrElse(localDateTime -> {
      alarmDate.setText(localDateTime.toLocalDate().format(DateTimeFormatter.ofPattern("dd'.' MMM yyyy")));
      alarmTime.setText(localDateTime.toLocalTime().format(DateTimeFormatter.ISO_LOCAL_TIME));
    }, () -> {
      alarmDate.setText("");
      alarmTime.setText("");
    });
  }

  /**
   * creates a new Dialog to show all the alarms
   *
   * @see ShowAlarmsDialog
   */
  private void showAllAlarmsBtnActionPerformed() {
    ShowAlarmsDialog showAlarmsDialog = new ShowAlarmsDialog(this);
    showAlarmsDialog.showDialog();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    var nextAlarmPanel = new JPanel();
    alarmDate = new JTextField();
    alarmTime = new JTextField();
    var dateLbl = new DateLabel();
    var timeLbl = new TimeLabel();
    var showAllAlarmsBtn = new JButton();
    var setAlarmBtn = new JButton();
    buttonPanel = new OkAndCancelBtnPanel();

    //======== this ========
    setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
    setResizable(false);
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    setUndecorated(true);
    var contentPane = getContentPane();
    contentPane.setLayout(new GridBagLayout());
    ((GridBagLayout) contentPane.getLayout()).columnWidths = new int[]{195, 200, 0};
    ((GridBagLayout) contentPane.getLayout()).rowHeights = new int[]{15, 70, 70, 45, 25, 40, 0};
    ((GridBagLayout) contentPane.getLayout()).columnWeights = new double[]{0.0, 1.0, 1.0E-4};
    ((GridBagLayout) contentPane.getLayout()).rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0E-4};

    //======== nextAlarmPanel ========
    {
      nextAlarmPanel.setBorder(new TitledBorder(new BevelBorder(BevelBorder.RAISED), "Next Alarm", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
          new Font("Segoe UI", Font.PLAIN, 18), new Color(153, 153, 153)));
      nextAlarmPanel.setLayout(new BorderLayout());

      //---- alarmDate ----
      alarmDate.setEditable(false);
      alarmDate.setFont(new Font("Segoe UI", Font.PLAIN, 14));
      alarmDate.setBorder(new TitledBorder(BorderFactory.createEmptyBorder(), "Date", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
          new Font("Segoe UI", Font.PLAIN, 14), new Color(204, 204, 204)));
      nextAlarmPanel.add(alarmDate, BorderLayout.NORTH);

      //---- alarmTime ----
      alarmTime.setEditable(false);
      alarmTime.setFont(new Font("Segoe UI", Font.PLAIN, 14));
      alarmTime.setBorder(new TitledBorder(BorderFactory.createEmptyBorder(), "Time", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
          new Font("Segoe UI", Font.PLAIN, 14), new Color(204, 204, 204)));
      nextAlarmPanel.add(alarmTime, BorderLayout.SOUTH);
    }
    contentPane.add(nextAlarmPanel, new GridBagConstraints(0, 1, 1, 2, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 10, 5, 15), 0, 0));

    //---- dateLbl ----
    dateLbl.setFont(dateLbl.getFont().deriveFont(dateLbl.getFont().getStyle() | Font.BOLD, dateLbl.getFont().getSize() + 12f));
    dateLbl.setBorder(new TitledBorder(new BevelBorder(BevelBorder.RAISED), "Current Date", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
        new Font("Segoe UI", Font.PLAIN, 18), new Color(153, 153, 153)));
    dateLbl.setHorizontalAlignment(SwingConstants.CENTER);
    contentPane.add(dateLbl, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 10, 5, 10), 0, 0));

    //---- timeLbl ----
    timeLbl.setFont(timeLbl.getFont().deriveFont(timeLbl.getFont().getStyle() | Font.BOLD, timeLbl.getFont().getSize() + 12f));
    timeLbl.setBorder(new TitledBorder(new BevelBorder(BevelBorder.RAISED), "Current Time", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
        new Font("Segoe UI", Font.PLAIN, 18), new Color(153, 153, 153)));
    timeLbl.setHorizontalAlignment(SwingConstants.CENTER);
    contentPane.add(timeLbl, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 10, 5, 10), 0, 0));

    //---- showAllAlarmsBtn ----
    showAllAlarmsBtn.setText("Show all Alarms");
    showAllAlarmsBtn.setFont(showAllAlarmsBtn.getFont().deriveFont(showAllAlarmsBtn.getFont().getSize() + 12f));
    showAllAlarmsBtn.addActionListener(e -> showAllAlarmsBtnActionPerformed());
    contentPane.add(showAllAlarmsBtn, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 10, 5, 15), 0, 0));

    //---- setAlarmBtn ----
    setAlarmBtn.setText("New Alarm");
    setAlarmBtn.setFont(setAlarmBtn.getFont().deriveFont(setAlarmBtn.getFont().getSize() + 12f));
    setAlarmBtn.addActionListener(e -> setAlarmBtnActionPerformed());
    contentPane.add(setAlarmBtn, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 10, 5, 10), 0, 0));
    contentPane.add(buttonPanel, new GridBagConstraints(0, 5, 2, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 10), 0, 0));
    pack();
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JTextField alarmDate;
  private JTextField alarmTime;
  private OkAndCancelBtnPanel buttonPanel;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
