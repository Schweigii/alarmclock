package alarmclock.gui.generalguicomponents;

import alarmclock.gui.generalguicomponents.checkboxlist.CheckboxListItem;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * This class represents a list item for a {@link javax.swing.JList}
 */
public class LocalDateTimeListItem implements CheckboxListItem {
  /**
   * The date time
   */
  private LocalDateTime dateTime;
  /**
   * The selection of the checkbox
   */
  private boolean selected = false;

  /**
   * This constructor is used initialize the date time
   *
   * @param dateTime the date time
   */
  public LocalDateTimeListItem(LocalDateTime dateTime) {
    this.dateTime = dateTime;
  }

  @Override
  public boolean isSelected() {
    return selected;
  }

  @Override
  public void setSelected(boolean selected) {
    this.selected = selected;
  }

  public LocalDateTime getDateTime() {
    return dateTime;
  }

  @Override
  public String toString() {
    return dateTime.format(DateTimeFormatter.ofPattern("'Date: ' dd'.' MMM yyyy   'Time: ' h:mm a", Locale.US));
  }
}
