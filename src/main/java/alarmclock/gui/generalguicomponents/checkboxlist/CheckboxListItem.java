package alarmclock.gui.generalguicomponents.checkboxlist;

/**
 * This interface must be implemented by any class that should be used in a {@link javax.swing.JList} components
 * with a checkbox on the left side
 */
public interface CheckboxListItem {
  /**
   * @return if checkbox is selected or not
   */
  boolean isSelected();

  /**
   * sets the selection of the checkbox of the value
   *
   * @param selected if the checkbox is should be selected
   */
  void setSelected(boolean selected);

  /**
   * @return how the list items should be shown
   */
  String toString();
}
