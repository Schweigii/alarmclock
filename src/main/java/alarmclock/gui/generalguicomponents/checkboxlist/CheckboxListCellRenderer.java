package alarmclock.gui.generalguicomponents.checkboxlist;

import javax.swing.*;
import java.awt.*;

/**
 * This class represents a Cell renderer for a list with a checkbox component
 *
 * @param <T> the Type of the list values
 */
public class CheckboxListCellRenderer<T extends CheckboxListItem> extends JCheckBox implements ListCellRenderer<T> {

  @Override
  public Component getListCellRendererComponent(JList<? extends T> list, T value, int index, boolean isSelected, boolean cellHasFocus) {
    setEnabled(list.isEnabled());
    setSelected(value.isSelected());
    setFont(list.getFont());
    setBackground(list.getBackground());
    setForeground(list.getForeground());
    setText(value.toString());
    return this;
  }
}
