package alarmclock.gui.generalguicomponents;

import javax.swing.*;
import java.awt.*;

/**
 * This class sets up general GUI-instructions for a dialog
 */
public abstract class GeneralAbstractJDialog extends JDialog {

  /**
   * This constructor is used to initialize the dialog
   *
   * @param owner the owner of this dialog
   */
  protected GeneralAbstractJDialog(Window owner) {
    super(owner);
    init();
  }

  /**
   * This constructor is used to initialize the dialog without an owner
   */
  protected GeneralAbstractJDialog() {
    init();
  }

  /**
   * disposes the dialog
   */
  public void cancel() {
    dispose();
  }

  /**
   * sets the dialog visible
   */
  public void showDialog() {
    setVisible(true);
  }

  /**
   * sets the basic operations for this dialog
   */
  private void init() {
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    setModal(true);
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
    setUndecorated(true);
    setResizable(false);
  }
}
