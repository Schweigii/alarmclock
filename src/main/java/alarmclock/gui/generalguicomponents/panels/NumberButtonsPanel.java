/*
 * Created by JFormDesigner on Fri May 22 22:51:46 CEST 2020
 */

package alarmclock.gui.generalguicomponents.panels;

import javax.swing.*;
import java.awt.*;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * @author Manuel Schweighofer
 * This panel has the numbers 1-9, ":" and a delete button to act as a num-pad for touch screens
 */
public class NumberButtonsPanel extends JPanel {
  /**
   * the consumer to set the new character of the buttonpress
   */
  private final Consumer<String> consumer;
  /**
   * the supplier to provide the current set string
   */
  private final Supplier<String> supplier;

  /**
   * This constructor is used to initialize the consumer and supplier as well as the GUI-Components
   *
   * @param consumer the consumer
   * @param supplier the supplier
   */
  public NumberButtonsPanel(Consumer<String> consumer, Supplier<String> supplier) {
    this.consumer = consumer;
    this.supplier = supplier;
    initComponents();
    firstInit();
  }

  /**
   * the custom init method to set the action listeners for every button
   */
  private void firstInit() {
    oneBtn.addActionListener(e -> consumer.accept(nonNullString(supplier.get()) + 1));
    twoBtn.addActionListener(e -> consumer.accept(nonNullString(supplier.get()) + 2));
    threeBtn.addActionListener(e -> consumer.accept(nonNullString(supplier.get()) + 3));
    fourBtn.addActionListener(e -> consumer.accept(nonNullString(supplier.get()) + 4));
    fiveBtn.addActionListener(e -> consumer.accept(nonNullString(supplier.get()) + 5));
    sixBtn.addActionListener(e -> consumer.accept(nonNullString(supplier.get()) + 6));
    sevenBtn.addActionListener(e -> consumer.accept(nonNullString(supplier.get()) + 7));
    eightBtn.addActionListener(e -> consumer.accept(nonNullString(supplier.get()) + 8));
    nineBtn.addActionListener(e -> consumer.accept(nonNullString(supplier.get()) + 9));
    zeroBtn.addActionListener(e -> consumer.accept(nonNullString(supplier.get()) + 0));
    colonBtn.addActionListener(e -> consumer.accept(nonNullString(supplier.get()) + ":"));
    backspaceBtn.addActionListener(e -> consumer.accept(nonNullString(supplier.get()).isEmpty() ?
        supplier.get() : supplier.get().substring(0, supplier.get().length() - 1)));
  }

  /**
   * @param supplierString the string of the supplier
   * @return a non null string
   */
  private String nonNullString(String supplierString) {
    return supplierString != null ? supplierString : "";
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    oneBtn = new JButton();
    twoBtn = new JButton();
    threeBtn = new JButton();
    fourBtn = new JButton();
    fiveBtn = new JButton();
    sixBtn = new JButton();
    sevenBtn = new JButton();
    eightBtn = new JButton();
    nineBtn = new JButton();
    zeroBtn = new JButton();
    colonBtn = new JButton();
    backspaceBtn = new JButton();

    //======== this ========
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[]{36, 0, 0, 0, 0, 0, 0};
    ((GridBagLayout) getLayout()).rowHeights = new int[]{0, 0, 0};
    ((GridBagLayout) getLayout()).columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
    ((GridBagLayout) getLayout()).rowWeights = new double[]{0.0, 0.0, 1.0E-4};

    //---- oneBtn ----
    oneBtn.setText("1");
    oneBtn.setMaximumSize(new Dimension(30, 30));
    oneBtn.setMinimumSize(new Dimension(30, 30));
    oneBtn.setPreferredSize(new Dimension(50, 30));
    oneBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    oneBtn.setForeground(Color.black);
    add(oneBtn, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));

    //---- twoBtn ----
    twoBtn.setText("2");
    twoBtn.setMaximumSize(new Dimension(30, 30));
    twoBtn.setMinimumSize(new Dimension(30, 30));
    twoBtn.setPreferredSize(new Dimension(50, 30));
    twoBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    twoBtn.setForeground(Color.black);
    add(twoBtn, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));

    //---- threeBtn ----
    threeBtn.setText("3");
    threeBtn.setMaximumSize(new Dimension(30, 30));
    threeBtn.setMinimumSize(new Dimension(30, 30));
    threeBtn.setPreferredSize(new Dimension(50, 30));
    threeBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    threeBtn.setForeground(Color.black);
    add(threeBtn, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));

    //---- fourBtn ----
    fourBtn.setText("4");
    fourBtn.setMaximumSize(new Dimension(30, 30));
    fourBtn.setMinimumSize(new Dimension(30, 30));
    fourBtn.setPreferredSize(new Dimension(50, 30));
    fourBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    fourBtn.setForeground(Color.black);
    add(fourBtn, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));

    //---- fiveBtn ----
    fiveBtn.setText("5");
    fiveBtn.setMaximumSize(new Dimension(30, 30));
    fiveBtn.setMinimumSize(new Dimension(30, 30));
    fiveBtn.setPreferredSize(new Dimension(50, 30));
    fiveBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    fiveBtn.setForeground(Color.black);
    add(fiveBtn, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));

    //---- sixBtn ----
    sixBtn.setText("6");
    sixBtn.setMaximumSize(new Dimension(30, 30));
    sixBtn.setMinimumSize(new Dimension(30, 30));
    sixBtn.setPreferredSize(new Dimension(50, 30));
    sixBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    sixBtn.setForeground(Color.black);
    add(sixBtn, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

    //---- sevenBtn ----
    sevenBtn.setText("7");
    sevenBtn.setMaximumSize(new Dimension(30, 30));
    sevenBtn.setMinimumSize(new Dimension(30, 30));
    sevenBtn.setPreferredSize(new Dimension(50, 30));
    sevenBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    sevenBtn.setForeground(Color.black);
    add(sevenBtn, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 5), 0, 0));

    //---- eightBtn ----
    eightBtn.setText("8");
    eightBtn.setMaximumSize(new Dimension(30, 30));
    eightBtn.setMinimumSize(new Dimension(30, 30));
    eightBtn.setPreferredSize(new Dimension(50, 30));
    eightBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    eightBtn.setForeground(Color.black);
    add(eightBtn, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 5), 0, 0));

    //---- nineBtn ----
    nineBtn.setText("9");
    nineBtn.setMaximumSize(new Dimension(30, 30));
    nineBtn.setMinimumSize(new Dimension(30, 30));
    nineBtn.setPreferredSize(new Dimension(50, 30));
    nineBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    nineBtn.setForeground(Color.black);
    add(nineBtn, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 5), 0, 0));

    //---- zeroBtn ----
    zeroBtn.setText("0");
    zeroBtn.setMaximumSize(new Dimension(30, 30));
    zeroBtn.setMinimumSize(new Dimension(30, 30));
    zeroBtn.setPreferredSize(new Dimension(50, 30));
    zeroBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    zeroBtn.setForeground(Color.black);
    add(zeroBtn, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 5), 0, 0));

    //---- colonBtn ----
    colonBtn.setText(":");
    colonBtn.setMaximumSize(new Dimension(30, 30));
    colonBtn.setMinimumSize(new Dimension(30, 30));
    colonBtn.setPreferredSize(new Dimension(50, 30));
    colonBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    colonBtn.setForeground(Color.black);
    add(colonBtn, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 5), 0, 0));

    //---- backspaceBtn ----
    backspaceBtn.setMaximumSize(new Dimension(30, 30));
    backspaceBtn.setMinimumSize(new Dimension(30, 30));
    backspaceBtn.setPreferredSize(new Dimension(50, 30));
    backspaceBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    backspaceBtn.setIcon(new ImageIcon(getClass().getResource("/backspace.png")));
    add(backspaceBtn, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JButton oneBtn;
  private JButton twoBtn;
  private JButton threeBtn;
  private JButton fourBtn;
  private JButton fiveBtn;
  private JButton sixBtn;
  private JButton sevenBtn;
  private JButton eightBtn;
  private JButton nineBtn;
  private JButton zeroBtn;
  private JButton colonBtn;
  private JButton backspaceBtn;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
