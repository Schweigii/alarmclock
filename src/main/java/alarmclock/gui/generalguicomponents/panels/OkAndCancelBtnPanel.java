/*
 * Created by JFormDesigner on Tue Apr 07 15:43:27 CEST 2020
 */

package alarmclock.gui.generalguicomponents.panels;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * @author Manuel Schweighofer
 * This panel has a cancel and ok button that already have a listener for changing the {@link GUIAction} correspondingly
 */
public class OkAndCancelBtnPanel extends JPanel {
  private GUIAction action = GUIAction.CANCEL;

  /**
   * This constructor initializes the GUI-components
   */
  public OkAndCancelBtnPanel() {
    initComponents();
  }

  /**
   * adds an {@link ActionListener} for the ok button to the list of action listeners
   *
   * @param l the listener
   */
  public void addOkActionListener(ActionListener l) {
    okButton.addActionListener(l);
  }

  /**
   * adds an {@link ActionListener} for the cancel button to the list of action listeners
   *
   * @param l the listener
   */
  public void addCancelActionListener(ActionListener l) {
    cancelButton.addActionListener(l);
  }

  /**
   * hides the ok button if its not needed for the dialog
   */
  public void hideOkButton() {
    okButton.setVisible(false);
  }

  /**
   * disables or enables the ok button
   *
   * @param enabled the boolean
   */
  public void setOkButtonEnabled(boolean enabled) {
    okButton.setEnabled(enabled);
  }

  private void okButtonActionPerformed() {
    action = GUIAction.OK;
  }

  private void cancelButtonActionPerformed() {
    action = GUIAction.CANCEL;
  }

  /**
   * @return the current set action
   */
  public GUIAction getAction() {
    return action;
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    okButton = new JButton();
    cancelButton = new JButton();

    //======== this ========
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[]{0, 145, 90, 0};
    ((GridBagLayout) getLayout()).rowHeights = new int[]{50, 0};
    ((GridBagLayout) getLayout()).columnWeights = new double[]{1.0, 0.0, 0.0, 1.0E-4};
    ((GridBagLayout) getLayout()).rowWeights = new double[]{0.0, 1.0E-4};

    //---- okButton ----
    okButton.setIcon(new ImageIcon(getClass().getResource("/check.png")));
    okButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    okButton.addActionListener(e -> okButtonActionPerformed());
    add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 5), 0, 0));

    //---- cancelButton ----
    cancelButton.setIcon(new ImageIcon(getClass().getResource("/cross.png")));
    cancelButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    cancelButton.addActionListener(e -> cancelButtonActionPerformed());
    add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JButton okButton;
  private JButton cancelButton;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
