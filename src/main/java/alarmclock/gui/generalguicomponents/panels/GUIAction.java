package alarmclock.gui.generalguicomponents.panels;

/**
 * This enum is used to identify the action taken when the buttons of a
 * {@link OkAndCancelBtnPanel} are pressed
 */
public enum GUIAction {
  OK,
  CANCEL,
  ;
}
