package alarmclock.gui.generalguicomponents.labels.datetime;

import java.awt.event.ActionEvent;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

/**
 * This class represents a label with time
 */
public class TimeLabel extends ClockLabel {

  @Override
  public void actionPerformed(ActionEvent e) {
    setText(LocalTime.now(clock).format(DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM)));
  }
}
