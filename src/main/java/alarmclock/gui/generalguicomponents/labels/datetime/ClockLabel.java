package alarmclock.gui.generalguicomponents.labels.datetime;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.time.Clock;

/**
 * This class represents a label that updates every second
 */
public abstract class ClockLabel extends JLabel implements ActionListener {
  /**
   * the {@link Clock} object to take the time from
   */
  protected final Clock clock;

  /**
   * This constructor is used to initialize the clock and the 1 second timer
   */
  public ClockLabel() {
    this.clock = Clock.systemDefaultZone();
    Timer timer = new Timer(1000, this);
    timer.start();
  }

}
