package alarmclock.gui.generalguicomponents.labels.datetime;

import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * This class represents a label with a date
 */
public class DateLabel extends ClockLabel {

  @Override
  public void actionPerformed(ActionEvent e) {
    setText(LocalDate.now(clock).format(DateTimeFormatter.ofPattern("dd'.' MMM yyyy")));
  }
}
