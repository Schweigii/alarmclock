/*
 * Created by JFormDesigner on Wed May 06 15:55:04 CEST 2020
 */

package alarmclock.gui.voicerecognition;

import alarmclock.logiclayer.manager.voicerecognition.SpeechEntryManager;
import alarmclock.gui.generalguicomponents.GeneralAbstractJDialog;
import alarmclock.gui.generalguicomponents.panels.OkAndCancelBtnPanel;

import javax.swing.*;
import java.awt.*;

/**
 * @author Manuel Schweighofer
 * This Dialog is shown when the voice recognition is activated
 */
public class VoiceRecognitionActiveDialog extends GeneralAbstractJDialog {
  /**
   * The {@link SpeechEntryManager} instance that is used to deactivate the voice recognition dispose the dialog
   */
  private SpeechEntryManager speechEntryManager = SpeechEntryManager.getInstance();

  /**
   * This constructor is used to init the GUI-Components of the dialog and call the custom init method
   */
  public VoiceRecognitionActiveDialog() {
    super();
    initComponents();
    firstInit();
  }

  /**
   * The custom init method that does a bunch of stuff like setting location and size
   */
  private void firstInit() {
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    setSize(screenSize);
    setLocation(0, 0);
    buttonPanel.addCancelActionListener(e -> speechEntryManager.deactivate());
    buttonPanel.hideOkButton();
  }

  /**
   * sets the label of the voice recognition to a custom string
   *
   * @param text the String to set
   */
  public void setVoiceRecTextLbl(String text) {
    voiceRecTextLbl.setText(text);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    var voiceRecLbl = new JLabel();
    voiceRecTextLbl = new JLabel();
    buttonPanel = new OkAndCancelBtnPanel();

    //======== this ========
    setAlwaysOnTop(true);
    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    setModal(false);
    setModalExclusionType(Dialog.ModalExclusionType.NO_EXCLUDE);
    var contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //---- voiceRecLbl ----
    voiceRecLbl.setText("<--Voice Recognition is active-->");
    voiceRecLbl.setHorizontalAlignment(SwingConstants.CENTER);
    voiceRecLbl.setFont(voiceRecLbl.getFont().deriveFont(voiceRecLbl.getFont().getSize() + 14f));
    contentPane.add(voiceRecLbl, BorderLayout.NORTH);

    //---- voiceRecTextLbl ----
    voiceRecTextLbl.setHorizontalAlignment(SwingConstants.CENTER);
    voiceRecTextLbl.setFont(voiceRecTextLbl.getFont().deriveFont(voiceRecTextLbl.getFont().getSize() + 14f));
    contentPane.add(voiceRecTextLbl, BorderLayout.CENTER);
    contentPane.add(buttonPanel, BorderLayout.SOUTH);
    pack();
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel voiceRecTextLbl;
  private OkAndCancelBtnPanel buttonPanel;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
