package alarmclock.gui;

import alarmclock.logiclayer.manager.alarmmanager.AlarmManager;

import java.util.Timer;
import java.util.TimerTask;

/**
 * This class holds a private class that is used as a Clock and updates every second
 */
public class AlarmTask {

  /**
   * This Constructor creates a new {@link Timer} object and schedules the {@link UpdateAlarmTask} to update every second
   */
  public AlarmTask() {
    Timer timer = new Timer();
    timer.schedule(new UpdateAlarmTask(), 0, 1000);
  }

  /**
   * The private static inner class that extends {@link TimerTask}
   * checks for alarms every second and triggers them
   */
  private static class UpdateAlarmTask extends TimerTask {
    /**
     * The {@link AlarmManager} instance to check and trigger the alarm
     */
    private AlarmManager alarmManager = AlarmManager.getInstance();

    @Override
    public void run() {
      if (alarmManager.checkAlarm()) {
        alarmManager.triggerAlarm();
      }
    }
  }

}
