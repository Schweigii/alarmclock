/*
 * Created by JFormDesigner on Fri May 22 09:04:10 CEST 2020
 */

package alarmclock.gui.showalarms;

import alarmclock.gui.generalguicomponents.GeneralAbstractJDialog;
import alarmclock.gui.generalguicomponents.panels.OkAndCancelBtnPanel;

import javax.swing.border.BevelBorder;
import java.awt.*;

/**
 * @author Manuel Schweighofer
 * This Dialog shows all set alarms
 */
public class ShowAlarmsDialog extends GeneralAbstractJDialog {
  /**
   * This constructor is used to init the GUI-components and call the custom init method afterwards
   *
   * @param owner the owner of this dialog
   */
  public ShowAlarmsDialog(Window owner) {
    super(owner);
    initComponents();
    firstInit(owner);
  }

  /**
   * The custom init method that does a bunch of stuff like setting location and size
   *
   * @param owner the owner of the dialog
   */
  private void firstInit(Window owner) {
    setSize(owner.getSize());
    setLocation(owner.getLocation());
    buttonPanel.hideOkButton();
    buttonPanel.addCancelActionListener(e -> cancel());
    showAlarmsPanel.reInit();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    showAlarmsPanel = new ShowAlarmsPanel();
    buttonPanel = new OkAndCancelBtnPanel();

    //======== this ========
    var contentPane = getContentPane();
    contentPane.setLayout(new GridBagLayout());
    ((GridBagLayout) contentPane.getLayout()).columnWidths = new int[]{0, 0};
    ((GridBagLayout) contentPane.getLayout()).rowHeights = new int[]{0, 0, 0};
    ((GridBagLayout) contentPane.getLayout()).columnWeights = new double[]{1.0, 1.0E-4};
    ((GridBagLayout) contentPane.getLayout()).rowWeights = new double[]{1.0, 0.0, 1.0E-4};

    //---- showAlarmsPanel ----
    showAlarmsPanel.setBorder(new BevelBorder(BevelBorder.RAISED));
    contentPane.add(showAlarmsPanel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(10, 10, 5, 10), 0, 0));
    contentPane.add(buttonPanel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 10), 0, 0));
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private ShowAlarmsPanel showAlarmsPanel;
  private OkAndCancelBtnPanel buttonPanel;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
