/*
 * Created by JFormDesigner on Fri May 22 09:08:23 CEST 2020
 */

package alarmclock.gui.showalarms;

import alarmclock.logiclayer.manager.voicerecognition.exceptions.InvalidAlarmDateTimeException;
import alarmclock.gui.choosealarm.ChooseAlarmDialog;
import alarmclock.gui.generalguicomponents.checkboxlist.CheckboxListCellRenderer;
import alarmclock.gui.generalguicomponents.LocalDateTimeListItem;
import alarmclock.gui.generalguicomponents.panels.GUIAction;
import alarmclock.logiclayer.manager.alarmmanager.AlarmManager;
import alarmclock.logiclayer.manager.obersvers.AlarmObserver;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.stream.IntStream;

/**
 * @author Manuel Schweighofer
 * This Panel is used to show the current alarms
 * It has functionallity to delete current alarm date times or add new ones
 */
class ShowAlarmsPanel extends JPanel implements AlarmObserver {
  /**
   * The {@link AlarmManager} instance used to get the alarms from, delete or add new ones
   */
  private AlarmManager alarmManager = AlarmManager.getInstance();

  /**
   * This constructor is used to create the GUI-components and calls the custom init method afterwards
   */
  ShowAlarmsPanel() {
    initComponents();
    firstInit();
  }

  /**
   * The custom init method that does a bunch of stuff like setting the custom {@link CheckboxListCellRenderer}
   */
  private void firstInit() {
    alarmList.setCellRenderer(new CheckboxListCellRenderer<>());
    alarmList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    alarmManager.addObserver(this);
  }

  /**
   * reInits the <code>alarmList</code> object with the changed data and maps it accordingly
   */
  void reInit() {
    alarmList.setListData(alarmManager.getAllAlarmDateTimes()
        .stream()
        .map(LocalDateTimeListItem::new)
        .toArray(LocalDateTimeListItem[]::new));
  }

  /**
   * The mouse clicked action event to select and deselect list items
   *
   * @param e the {@link MouseEvent}
   */
  private void alarmListMouseClicked(MouseEvent e) {
    int index = alarmList.locationToIndex(e.getPoint());
    if (index >= 0) {
      LocalDateTimeListItem item = alarmList.getModel().getElementAt(index);
      item.setSelected(!item.isSelected());
      alarmList.repaint(alarmList.getCellBounds(index, index));
    }
  }

  /**
   * The action event of the addBtn
   * Creates a new {@link ChooseAlarmDialog} to add a new alarm date time
   */
  private void addBtnActionPerformed() {
    ChooseAlarmDialog chooseAlarmDialog = new ChooseAlarmDialog(SwingUtilities.getWindowAncestor(this));
    chooseAlarmDialog.showDialog();
    if (chooseAlarmDialog.getAction() == GUIAction.OK) {
      try {
        alarmManager.setNewAlarmTime(chooseAlarmDialog.getSetAlarmDateTime());
      } catch (InvalidAlarmDateTimeException e) {
        System.out.println(e.getMessage());
      }
    }
  }

  /**
   * The action event of the deleteBtn
   * deletes the selected alarm date times from the list
   */
  private void deleteBtnActionPerformed() {
    ListModel<LocalDateTimeListItem> alarmListModel = alarmList.getModel();
    IntStream.range(0, alarmListModel.getSize())
        .mapToObj(alarmListModel::getElementAt)
        .filter(LocalDateTimeListItem::isSelected)
        .map(LocalDateTimeListItem::getDateTime)
        .forEach(alarmManager::deleteAlarmTime);
  }

  /**
   * reInits the list when the alarm date times change
   */
  @Override
  public void update() {
    reInit();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    var addBtn = new JButton();
    var deleteBtn = new JButton();
    var alarmListScrollPane = new JScrollPane();
    alarmList = new JList<>();

    //======== this ========
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[]{70, 80, 0, 0};
    ((GridBagLayout) getLayout()).rowHeights = new int[]{45, 0, 0};
    ((GridBagLayout) getLayout()).columnWeights = new double[]{0.0, 0.0, 1.0, 1.0E-4};
    ((GridBagLayout) getLayout()).rowWeights = new double[]{0.0, 1.0, 1.0E-4};

    //---- addBtn ----
    addBtn.setIcon(new ImageIcon(getClass().getResource("/add.png")));
    addBtn.setBorder(new BevelBorder(BevelBorder.RAISED, new Color(23, 103, 32), new Color(23, 102, 31), new Color(23, 102, 31), new Color(23, 102, 31)));
    addBtn.addActionListener(e -> addBtnActionPerformed());
    add(addBtn, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

    //---- deleteBtn ----
    deleteBtn.setIcon(new ImageIcon(getClass().getResource("/delete.png")));
    deleteBtn.setBorder(new BevelBorder(BevelBorder.RAISED, new Color(255, 73, 73), new Color(255, 73, 73), new Color(255, 73, 73), new Color(255, 73, 73)));
    deleteBtn.addActionListener(e -> deleteBtnActionPerformed());
    add(deleteBtn, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 10, 5, 0), 0, 0));

    //======== alarmListScrollPane ========
    {

      //---- alarmList ----
      alarmList.setFont(new Font("Segoe UI", Font.PLAIN, 14));
      alarmList.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
          alarmListMouseClicked(e);
        }
      });
      alarmListScrollPane.setViewportView(alarmList);
    }
    add(alarmListScrollPane, new GridBagConstraints(0, 1, 3, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JList<LocalDateTimeListItem> alarmList;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
