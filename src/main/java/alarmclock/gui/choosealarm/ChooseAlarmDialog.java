/*
 * Created by JFormDesigner on Tue Apr 07 15:41:58 CEST 2020
 */

package alarmclock.gui.choosealarm;

import alarmclock.gui.generalguicomponents.GeneralAbstractJDialog;
import alarmclock.gui.generalguicomponents.panels.GUIAction;
import alarmclock.gui.generalguicomponents.panels.OkAndCancelBtnPanel;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.time.LocalDateTime;

/**
 * @author Manuel Schweighofer
 * This dialog is used to set new alarms
 */
public class ChooseAlarmDialog extends GeneralAbstractJDialog {
  /**
   * the alarm date time that has been newly set
   */
  private LocalDateTime setAlarmDateTime;

  /**
   * This constructor is used to init the GUI-components and call the custom init method afterwards
   *
   * @param owner the owner of this dialog
   */
  public ChooseAlarmDialog(Window owner) {
    super(owner);
    initComponents();
    firstInit(owner);
  }

  /**
   * The custom init method that does a bunch of stuff like setting location and size
   *
   * @param owner the owner of the dialog
   */
  private void firstInit(Window owner) {
    setSize(owner.getSize());
    setLocation(owner.getLocation());
    buttonPanel.setOkButtonEnabled(false);
    buttonPanel.addCancelActionListener(e -> cancel());
    buttonPanel.addOkActionListener(e -> {
      setAlarmDateTime = LocalDateTime.of(setAlarmPanel.getSelectedDate(), setAlarmPanel.getSelectedTime());
      cancel();
    });
    setAlarmPanel.addTextFieldListener(new DocumentListener() {
      @Override
      public void insertUpdate(DocumentEvent e) {
        buttonPanel.setOkButtonEnabled(setAlarmPanel.areTextFieldsValid());
      }

      @Override
      public void removeUpdate(DocumentEvent e) {
        buttonPanel.setOkButtonEnabled(setAlarmPanel.areTextFieldsValid());
      }

      @Override
      public void changedUpdate(DocumentEvent e) {
        buttonPanel.setOkButtonEnabled(setAlarmPanel.areTextFieldsValid());
      }
    });
  }

  /**
   * @return the current set {@link GUIAction} of the dialog
   */
  public GUIAction getAction() {
    return buttonPanel.getAction();
  }

  public LocalDateTime getSetAlarmDateTime() {
    return setAlarmDateTime;
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    buttonPanel = new OkAndCancelBtnPanel();
    var contentPanel = new JPanel();
    setAlarmPanel = new ChooseAlarmPanel();

    //======== this ========
    var contentPane = getContentPane();
    contentPane.setLayout(new GridBagLayout());
    ((GridBagLayout) contentPane.getLayout()).columnWidths = new int[]{0, 0};
    ((GridBagLayout) contentPane.getLayout()).rowHeights = new int[]{0, 0, 0};
    ((GridBagLayout) contentPane.getLayout()).columnWeights = new double[]{1.0, 1.0E-4};
    ((GridBagLayout) contentPane.getLayout()).rowWeights = new double[]{1.0, 0.0, 1.0E-4};
    contentPane.add(buttonPanel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 10), 0, 0));

    //======== contentPanel ========
    {
      contentPanel.setLayout(new BorderLayout());
      contentPanel.add(setAlarmPanel, BorderLayout.CENTER);
    }
    contentPane.add(contentPanel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private OkAndCancelBtnPanel buttonPanel;
  private ChooseAlarmPanel setAlarmPanel;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
