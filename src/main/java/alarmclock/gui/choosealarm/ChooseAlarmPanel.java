/*
 * Created by JFormDesigner on Tue Apr 07 15:52:28 CEST 2020
 */

package alarmclock.gui.choosealarm;

import alarmclock.gui.generalguicomponents.panels.NumberButtonsPanel;
import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.DatePickerSettings;
import com.github.lgooddatepicker.components.TimePicker;
import com.github.lgooddatepicker.components.TimePickerSettings;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * @author Manuel Schweighofer
 * This panel contains the time picker and date picker as well as a number panel for the input of the time panel
 */
class ChooseAlarmPanel extends JPanel {
  /**
   * This constructor is used to initialize the GUI-components and calls the custom init method afterwards
   */
  ChooseAlarmPanel() {
    initComponents();
    firstInit();
  }

  /**
   * the custom init method which initializes the time and date picker
   */
  private void firstInit() {
    timePicker.getSettings().generatePotentialMenuTimes(TimePickerSettings.TimeIncrement.TenMinutes, null, null);
    initDatePicker();
  }

  /**
   * initializes a lot of stuff for the time and date picker
   */
  private void initDatePicker() {
    timePicker.getSettings().setMinimumToggleTimeMenuButtonWidthInPixels(50);
    timePicker.getComponentTimeTextField().setPreferredSize(new Dimension(100, 42));
    TimePickerSettings timeSetting = timePicker.getSettings();
    timeSetting.fontValidTime = timeSetting.fontValidTime.deriveFont(20f);
    timeSetting.fontInvalidTime = timeSetting.fontInvalidTime.deriveFont(20f);
    timeSetting.fontVetoedTime = timeSetting.fontVetoedTime.deriveFont(20f);

    datePicker.getComponentToggleCalendarButton().setPreferredSize(new Dimension(50, (int) datePicker.getComponentToggleCalendarButton().getPreferredSize().getHeight()));
    datePicker.getComponentDateTextField().setPreferredSize(new Dimension(100, 42));
    DatePickerSettings dateSetting = datePicker.getSettings();
    dateSetting.setFontValidDate(dateSetting.getFontValidDate().deriveFont(20f));
    dateSetting.setFontInvalidDate(dateSetting.getFontInvalidDate().deriveFont(20f));
    dateSetting.setFontVetoedDate(dateSetting.getFontInvalidDate().deriveFont(20f));
  }

  /**
   * adds a listener to the timePicker and datePicker object
   *
   * @param listener the {@link DocumentListener} to set
   */
  void addTextFieldListener(DocumentListener listener) {
    timePicker.getComponentTimeTextField().getDocument().addDocumentListener(listener);
    datePicker.getComponentDateTextField().getDocument().addDocumentListener(listener);
  }

  /**
   * @return if the time picker and date picker text fields are valid or not
   */
  boolean areTextFieldsValid() {
    return timePicker.isTextFieldValid() && datePicker.isTextFieldValid() &&
        timePicker.getText() != null && !timePicker.getText().isEmpty() &&
        datePicker.getText() != null && !datePicker.getText().isEmpty();
  }

  public LocalDate getSelectedDate() {
    return datePicker.getDate();
  }

  public LocalTime getSelectedTime() {
    return timePicker.getTime();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    datePicker = new DatePicker();
    timePicker = new TimePicker();
    var numberBtnsPanel = new NumberButtonsPanel(timePicker.getComponentTimeTextField()::setText, timePicker.getComponentTimeTextField()::getText);

    //======== this ========
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[]{0, 0};
    ((GridBagLayout) getLayout()).rowHeights = new int[]{20, 20, 56, 0};
    ((GridBagLayout) getLayout()).columnWeights = new double[]{1.0, 1.0E-4};
    ((GridBagLayout) getLayout()).rowWeights = new double[]{0.0, 0.0, 1.0, 1.0E-4};

    //---- datePicker ----
    datePicker.setBorder(new TitledBorder(new BevelBorder(BevelBorder.RAISED), "Choose Date", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
        new Font("Segoe UI", Font.PLAIN, 18), new Color(153, 153, 153)));
    add(datePicker, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 10, 0, 10), 0, 0));

    //---- timePicker ----
    timePicker.setBorder(new TitledBorder(new BevelBorder(BevelBorder.RAISED), "Choose Time", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
        new Font("Segoe UI", Font.PLAIN, 18), new Color(153, 153, 153)));
    add(timePicker, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 10, 0, 10), 0, 0));
    add(numberBtnsPanel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
        new Insets(10, 0, 10, 0), 0, 0));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private DatePicker datePicker;
  private TimePicker timePicker;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
