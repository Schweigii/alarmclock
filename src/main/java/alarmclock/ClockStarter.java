package alarmclock;

import alarmclock.gui.Clock;
import alarmclock.logiclayer.manager.voicerecognition.threadmanagement.VoiceRecognitionManager;
import alarmclock.logiclayer.manager.voicerecognition.threadmanagement.threads.RecogniserThread;
import alarmclock.logiclayer.manager.voicerecognition.threadmanagement.threads.RecognitionThread;
import edu.cmu.sphinx.api.Configuration;

import javax.swing.*;

public class ClockStarter {
  public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
    Configuration configuration = new Configuration();
    VoiceRecognitionManager voiceRecognitionManager = new VoiceRecognitionManager(VoiceRecognitionManager.VoiceRecognitionConfiguration.CUSTOM, configuration);
    voiceRecognitionManager.initRecogniserThread(new RecogniserThread(configuration));
    voiceRecognitionManager.initRecognitionThread(new RecognitionThread());
    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    Clock clock = new Clock();
    clock.setVisible(true);
  }
}
