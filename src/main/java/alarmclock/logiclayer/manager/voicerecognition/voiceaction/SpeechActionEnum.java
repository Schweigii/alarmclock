package alarmclock.logiclayer.manager.voicerecognition.voiceaction;

import alarmclock.logiclayer.manager.alarmmanager.AlarmManager;
import alarmclock.logiclayer.manager.texttospeech.SpeechManager;
import alarmclock.logiclayer.manager.voicerecognition.SpeechEntryManager;
import alarmclock.logiclayer.manager.voicerecognition.exceptions.InvalidAlarmDateTimeException;
import alarmclock.logiclayer.manager.voicerecognition.exceptions.InvalidInputException;
import alarmclock.logiclayer.manager.voicerecognition.inputparser.SpeechInputDateTimeParser;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * This enum executes commands on a {@link VocalInput}
 * The methods of a single enum value cannot be called directly, instead the action of which value is triggered is handled by the
 * {@link SpeechActionEnum#activate(VocalInput)} method, which tries to find keywords inside of a {@link VocalInput} to evaluate the correct action.
 */
public enum SpeechActionEnum {
  /**
   * This value activates the voice recognition
   */
  ACTIVATE("KEVIN") {
    @Override
    void doAction(VocalInput data) {
      speechEntryManager.activate();
    }
  },

  /**
   * This value sets an alarm at the date and time that is specified in the {@link VocalInput} string
   *
   * @see SpeechInputDateTimeParser#parseDateTimeHypothesis(java.lang.String) for more information on how the data is parsed
   */
  SET_ALARM("SET ALARM AT") {
    @Override
    void doAction(VocalInput data) throws InvalidInputException {
      String hypothesis = data.getHypothesis();
      String timeDateToParse = hypothesis.substring(hypothesis.lastIndexOf("AT") + 2).trim();
      try {
        LocalDateTime newAlarmDateTime = speechInputDateTimeParser.parseDateTimeHypothesis(timeDateToParse);
        speechManager.speakMessage("of course");
        TimeUnit.SECONDS.sleep(2);
        alarmManager.setNewAlarmTime(newAlarmDateTime);
        speechManager.speakAlarmSet(newAlarmDateTime);
      } catch (InvalidAlarmDateTimeException e) {
        e.speakException();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  },

  /**
   * This value outputs the next alarm through the TTS-Engine
   */
  NEXT_ALARM("WHEN IS THE NEXT ALARM") {
    @Override
    void doAction(VocalInput data) {
      alarmManager.getNextAlarm().ifPresentOrElse(speechManager::speakNextAlarm, () -> speechManager.speakMessage("There are no alarms currently set"));
    }
  },

  /**
   * This value outputs the current time through the TTS-Engine
   */
  TELL_TIME("WHAT TIME IS IT") {
    @Override
    void doAction(VocalInput data) {
      speechManager.speakTime();
    }
  },
  ;

  private final String speechRecognitionDescription;

  private static final SpeechInputDateTimeParser speechInputDateTimeParser = SpeechInputDateTimeParser.getInstance();
  private static final SpeechManager speechManager = SpeechManager.getInstance();
  private static final AlarmManager alarmManager = AlarmManager.getInstance();
  private static final SpeechEntryManager speechEntryManager = SpeechEntryManager.getInstance();

  SpeechActionEnum(String speechRecognitionDescription) {
    this.speechRecognitionDescription = speechRecognitionDescription;
  }

  abstract void doAction(VocalInput data) throws InvalidInputException;

  /**
   * This method can be called to iterate over the enum values and call the correct {@link SpeechActionEnum#doAction(VocalInput)} method depending on the {@link VocalInput}
   *
   * @param data the vocal input used to identify the action and execute it
   */
  public static void activate(VocalInput data) {
    if (data.getHypothesis().contains(ACTIVATE.speechRecognitionDescription)) {
      if (!speechEntryManager.isActive()) {
        try {
          ACTIVATE.doAction(data);
        } catch (InvalidInputException e) {
          e.speakException();
        }
      }
    } else if (speechEntryManager.isActive()) {
      speechEntryManager.setDialogText(data.getHypothesis());
      try {
        SpeechActionEnum actionEnum = Arrays.stream(values())
            .filter(speechActionEnum -> data.getHypothesis().contains(speechActionEnum.speechRecognitionDescription))
            .findFirst()
            .orElseThrow(InvalidInputException::new);
        actionEnum.doAction(data);
      } catch (InvalidInputException e) {
        e.speakException();
      } finally {
        speechEntryManager.deactivate();
      }
    }
  }
}
