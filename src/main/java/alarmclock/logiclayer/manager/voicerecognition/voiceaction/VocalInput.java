package alarmclock.logiclayer.manager.voicerecognition.voiceaction;

/**
 * This class represents an input from the voice recognition engine
 */
public class VocalInput {
  /**
   * The string parsed from the voice recognition
   */
  private final String hypothesis;

  public VocalInput(String hypothesis) {
    this.hypothesis = hypothesis;
  }

  public String getHypothesis() {
    return hypothesis;
  }
}
