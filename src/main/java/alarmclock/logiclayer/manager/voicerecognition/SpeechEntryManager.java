package alarmclock.logiclayer.manager.voicerecognition;

import alarmclock.logiclayer.manager.voicerecognition.threadmanagement.threads.RecogniserThread;
import alarmclock.logiclayer.manager.voicerecognition.threadmanagement.threads.RecognitionThread;
import edu.cmu.sphinx.api.SpeechResult;
import alarmclock.gui.voicerecognition.VoiceRecognitionActiveDialog;

import java.util.ArrayDeque;
import java.util.Optional;
import java.util.Queue;

/**
 * This class holds and manages the entries of the {@link SpeechResult} class
 * This is mainly used as a connection between Thread adding results and the Thread executing on those results
 * It also handles the use of the dialog that is shown on voice activation
 *
 * @see VoiceRecognitionActiveDialog
 * @see RecogniserThread
 * @see RecognitionThread
 */
public class SpeechEntryManager {
  /**
   * The queue that holds the {@link SpeechResult}
   * only one at a time, as the recognition thread operates at the same rate as the recogniser thread
   */
  private Queue<SpeechResult> speechResults = new ArrayDeque<>();
  /**
   * The state in which the voice recognition is currently in
   */
  private RecognitionMode mode = RecognitionMode.SLEEPING;
  /**
   * The dialog that is shown when the voice activation is in {@link RecognitionMode#ACTIVE}
   */
  private VoiceRecognitionActiveDialog dialog;

  SpeechEntryManager() {
  }

  private static class InstanceHolder {
    public static final SpeechEntryManager INSTANCE = new SpeechEntryManager();
  }

  /**
   * @return the single instance of {@link SpeechEntryManager}
   */
  public static SpeechEntryManager getInstance() {
    return InstanceHolder.INSTANCE;
  }

  /**
   * pulls a result from the queue
   *
   * @return an {@link Optional} of {@link SpeechResult}
   */
  public Optional<SpeechResult> getResult() {
    return Optional.ofNullable(speechResults.poll());
  }

  /**
   * adds a result to the queue
   *
   * @param result the result to add
   */
  public void addResult(SpeechResult result) {
    speechResults.add(result);
  }

  /**
   * @return if the current state of the voice recognition is active
   */
  public boolean isActive() {
    return mode == RecognitionMode.ACTIVE;
  }

  /**
   * sets the state of the voice recognition to active and shows the voice recognition dialog
   */
  public void activate() {
    mode = RecognitionMode.ACTIVE;
    dialog = new VoiceRecognitionActiveDialog();
    dialog.showDialog();
  }

  /**
   * sets the state of the voice recognition to sleeping, clears all leftover results and hides the voice recognition dialog
   */
  public void deactivate() {
    mode = RecognitionMode.SLEEPING;
    speechResults.clear();
    dialog.cancel();
  }

  /**
   * sets the text of the voice recognition dialog
   *
   * @param text the text to be set
   */
  public void setDialogText(String text) {
    if (isActive()) {
      dialog.setVoiceRecTextLbl(text);
      dialog.repaint();
    }
  }

  private enum RecognitionMode {
    ACTIVE("Kevin"),
    SLEEPING(""),
    ;

    private final String recognitionKey;

    RecognitionMode(String recognitionKey) {
      this.recognitionKey = recognitionKey;
    }

    public String getRecognitionKey() {
      return recognitionKey;
    }
  }
}
