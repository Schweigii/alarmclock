package alarmclock.logiclayer.manager.voicerecognition.inputparser;

import alarmclock.logiclayer.manager.voicerecognition.exceptions.InvalidInputException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * This class is used to parse voice input do {@link LocalDateTime}
 */
public class SpeechInputDateTimeParser {
  SpeechInputDateTimeParser() {
  }

  private static class InstanceHolder {
    public static final SpeechInputDateTimeParser INSTANCE = new SpeechInputDateTimeParser();
  }

  /**
   * @return the single instance of the {@link SpeechInputDateTimeParser} class
   */
  public static SpeechInputDateTimeParser getInstance() {
    return InstanceHolder.INSTANCE;
  }

  /**
   * Tries to parse a given hypothesis string to a {@link LocalDateTime} object
   * <p>
   * Input must look like 'SPEECHNUMBER [SPEECHNUMBER] [SPEECHNUMBER] SPEECHMERIDIAN (2 words) ["NEXT"] SPEECHDATE
   * whereas [] are optional parameters
   *
   * @param hypothesis the hypothesis of the voice input
   * @return the {@link LocalDateTime} object that represents the voice input
   * @throws InvalidInputException if the voice input isn't parseable
   */
  public LocalDateTime parseDateTimeHypothesis(String hypothesis) throws InvalidInputException {
    String[] splits = hypothesis.split(" ");
    if (splits.length < 4 || splits.length > 7) {
      throw new InvalidInputException();
    }

    try {
      String[] timeSplits = hypothesis.substring(0, hypothesis.indexOf("MIDDAY") + 6).split(" ");
      String[] daySplits = hypothesis.substring(hypothesis.indexOf("MIDDAY") + 7).split(" ");
      return LocalDateTime.of(formatDateHypothesis(daySplits), formattedInputToLocalTime(formatTimeHypothesis(timeSplits)));
    } catch (IndexOutOfBoundsException e) {
      throw new InvalidInputException();
    }
  }

  private LocalDate formattedInputToLocalDate(String input) throws InvalidInputException {
    return SpeechDate.getSpeechDateForHypothesis(input).getSpeechDate();
  }

  private LocalDate formatDateHypothesis(String[] hypothesis) throws InvalidInputException {
    if (hypothesis.length < 1 || hypothesis.length > 2) {
      throw new InvalidInputException();
    }

    return formattedInputToLocalDate(hypothesis[hypothesis.length - 1]);
  }

  private LocalTime formattedInputToLocalTime(String input) {
    return LocalTime.parse(input, DateTimeFormatter.ofPattern("H:mm", Locale.US));
  }

  private String formatTimeHypothesis(String[] hypothesis) throws InvalidInputException {
    if (hypothesis.length < 3 || hypothesis.length > 5) {
      throw new InvalidInputException();
    }

    SpeechMeridian meridian = SpeechMeridian.getMeridianForHypothesis(hypothesis[hypothesis.length - 2] + " " + hypothesis[hypothesis.length - 1]);
    int speechHour;
    int speechMinute;
    switch (hypothesis.length) {
      case 3:
        speechHour = SpeechNumber.getHourForHypothesis(hypothesis[0]).getNumber();
        return (speechHour + (meridian == SpeechMeridian.PM ? 12 : 0)) + ":" + "00";
      case 4:
        speechHour = SpeechNumber.getHourForHypothesis(hypothesis[0]).getNumber();
        speechMinute = SpeechNumber.getNumberForHypothesis(hypothesis[1]).getNumber();
        return (speechHour + (meridian == SpeechMeridian.PM ? 12 : 0)) + ":" + speechMinute;
      case 5:
        speechHour = SpeechNumber.getHourForHypothesis(hypothesis[0]).getNumber();
        speechMinute = SpeechNumber.getNumberForHypothesis(hypothesis[1]).getNumber() + SpeechNumber.getNumberForHypothesis(hypothesis[2]).getNumber();
        return (speechHour + (meridian == SpeechMeridian.PM ? 12 : 0)) + ":" + speechMinute;
      default:
        throw new InvalidInputException();
    }
  }
}