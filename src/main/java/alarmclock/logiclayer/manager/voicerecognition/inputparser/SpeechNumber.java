package alarmclock.logiclayer.manager.voicerecognition.inputparser;

import alarmclock.logiclayer.manager.voicerecognition.exceptions.InvalidInputException;

import java.util.Arrays;

/**
 * This enum is used to parse voice input to its number counterpart
 */
enum SpeechNumber {
  ZERO(0),
  ONE(1),
  TWO(2),
  THREE(3),
  FOUR(4),
  FIVE(5),
  SIX(6),
  SEVEN(7),
  EIGHT(8),
  NINE(9),
  TEN(10),
  ELEVEN(11),
  TWELVE(12),
  THIRTEEN(13),
  FOURTEEN(14),
  FIFTEEN(15),
  SIXTEEN(16),
  SEVENTEEN(17),
  EIGHTEEN(18),
  NINETEEN(19),
  TWENTY(20),
  THIRTY(30),
  FORTY(40),
  FIFTY(50),
  SIXTY(60),
  ;

  private final int number;

  SpeechNumber(int number) {
    this.number = number;
  }

  /**
   * @return the int number of the enum
   */
  public int getNumber() {
    return number;
  }

  /**
   * Tries to parse the hypothesis String to its int value counter part
   *
   * @param hypothesis the hypothesis of the voice input
   * @return the enum value that represents the voice input
   * @throws InvalidInputException if the voice input doesn't match any values in the enum
   */
  public static SpeechNumber getNumberForHypothesis(String hypothesis) throws InvalidInputException {
    return Arrays.stream(values())
        .filter(speechNumberParser -> speechNumberParser.name().equalsIgnoreCase(hypothesis))
        .findFirst()
        .orElseThrow(InvalidInputException::new);
  }

  /**
   * Same as {@link SpeechNumber#getNumberForHypothesis(String)}, but parses only numbers from 0 to 12 representing the hours of a day
   *
   * @param hypothesis the hypothesis of the voice input
   * @return the enum value that represents the voice input
   * @throws InvalidInputException if the voice input doesn't match any values or is not between 0 to 12
   */
  public static SpeechNumber getHourForHypothesis(String hypothesis) throws InvalidInputException {
    SpeechNumber hour = getNumberForHypothesis(hypothesis);
    if (hour.getNumber() > 12) {
      throw new InvalidInputException();
    }
    return hour;
  }
}
