package alarmclock.logiclayer.manager.voicerecognition.inputparser;

import alarmclock.logiclayer.manager.voicerecognition.exceptions.InvalidInputException;

import java.util.Arrays;

/**
 * This enum is used to parse voice input to its meridian counterpart
 */
enum SpeechMeridian {
  AM("before midday"),
  PM("after midday"),
  ;

  private final String recognition;

  SpeechMeridian(String recognition) {
    this.recognition = recognition;
  }

  /**
   * Tries to parse the hypothesis string to its meridian counterpart
   *
   * @param hypothesis the hypothesis of the voice input
   * @return the enum value that represents the voice input
   * @throws InvalidInputException if the voice input doesn't match any values in the enum
   */
  public static SpeechMeridian getMeridianForHypothesis(String hypothesis) throws InvalidInputException {
    return Arrays.stream(values())
        .filter(speechMeridian -> speechMeridian.recognition.equalsIgnoreCase(hypothesis))
        .findFirst()
        .orElseThrow(InvalidInputException::new);
  }
}
