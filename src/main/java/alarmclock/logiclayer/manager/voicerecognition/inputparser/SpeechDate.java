package alarmclock.logiclayer.manager.voicerecognition.inputparser;

import alarmclock.logiclayer.manager.voicerecognition.exceptions.InvalidInputException;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Arrays;

/**
 * This enum is used to parse voice input to its date counterpart
 */
enum SpeechDate {
  TODAY(LocalDate.now()),
  TOMORROW(LocalDate.now().plusDays(1)),
  MONDAY(LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.MONDAY))),
  TUESDAY(LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.TUESDAY))),
  WEDNESDAY(LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.WEDNESDAY))),
  THURSDAY(LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.THURSDAY))),
  FRIDAY(LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.FRIDAY))),
  SATURDAY(LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.SATURDAY))),
  SUNDAY(LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.SUNDAY))),
  ;

  private final LocalDate speechDate;

  SpeechDate(LocalDate speechDate) {
    this.speechDate = speechDate;
  }

  public LocalDate getSpeechDate() {
    return speechDate;
  }

  /**
   * Tries to parse the hypothesis string to its date counterpart
   *
   * @param hypothesis the hypothesis of the voice input
   * @return the enum value that represents the voice input
   * @throws InvalidInputException if the voice input doesn't match any values in the enum
   */
  public static SpeechDate getSpeechDateForHypothesis(String hypothesis) throws InvalidInputException {
    return Arrays.stream(values())
        .filter(date -> date.name().equalsIgnoreCase(hypothesis))
        .findFirst()
        .orElseThrow(InvalidInputException::new);
  }

}
