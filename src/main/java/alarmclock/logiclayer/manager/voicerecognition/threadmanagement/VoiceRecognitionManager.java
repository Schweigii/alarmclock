package alarmclock.logiclayer.manager.voicerecognition.threadmanagement;

import alarmclock.logiclayer.manager.voicerecognition.threadmanagement.threads.RecogniserThread;
import alarmclock.logiclayer.manager.voicerecognition.threadmanagement.threads.RecognitionThread;
import edu.cmu.sphinx.api.Configuration;

/**
 * This class holds and manages the states of a {@link RecogniserThread} and {@link RecognitionThread} instance
 */
public class VoiceRecognitionManager {
  /**
   * The thread that manages voice input
   */
  private RecogniserThread recogniserThread;
  /**
   * The thread that operates on the voice input of <code>recogniserThread</code>
   */
  private RecognitionThread recognitionThread;

  /**
   * This constructor is used to initialize the <code>configuration</code> with the <code>config</code>
   *
   * @param config        the {@link VoiceRecognitionConfiguration} enum type that is used to init the configuration object
   * @param configuration the {@link Configuration} of the voice recognition
   */
  public VoiceRecognitionManager(VoiceRecognitionConfiguration config, Configuration configuration) {
    config.initConfig(configuration);
  }

  /**
   * initializes the <code>recogniserThread</code> and starts the recognition, if the thread isn't already started
   *
   * @param recogniserThread the thread to handle the voice input
   */
  public void initRecogniserThread(RecogniserThread recogniserThread) {
    this.recogniserThread = recogniserThread;
    startRecogniserThread();
  }

  /**
   * initializes the <code>recognitionThread</code> and starts the recognition, if the thread isn't already started
   *
   * @param recognitionThread the thread to operate on the voice input
   */
  public void initRecognitionThread(RecognitionThread recognitionThread) {
    this.recognitionThread = recognitionThread;
    startRecognitionThread();
  }

  private void startRecogniserThread() {
    if (isThreadDead(recogniserThread)) {
      if (recogniserThread.startRecognition()) {
        recogniserThread.setDaemon(true);
        recogniserThread.start();
      }
    }
  }

  private void startRecognitionThread() {
    if (isThreadDead(recognitionThread)) {
      recognitionThread.setDaemon(true);
      recognitionThread.start();
    }
  }

  private boolean isThreadDead(Thread threadToCheck) {
    return !threadToCheck.isAlive();
  }

  /**
   * This enum is used to set AcousticModelPath, DictionaryPath and LanguageModelPath of a {@link Configuration} object
   */
  public enum VoiceRecognitionConfiguration {
    STANDARD {
      @Override
      void initConfig(Configuration configuration) {
        configuration.setAcousticModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us");
        configuration.setDictionaryPath("resource:/edu/cmu/sphinx/models/en-us/cmudict-en-us.dict");
        configuration.setLanguageModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us.lm.bin");
      }
    },
    CUSTOM {
      @Override
      void initConfig(Configuration configuration) {
        configuration.setAcousticModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us");
        configuration.setDictionaryPath("resource:/9525.dic");
        configuration.setLanguageModelPath("resource:/9525.lm");
      }
    };

    abstract void initConfig(Configuration configuration);
  }
}
