package alarmclock.logiclayer.manager.voicerecognition.threadmanagement.threads;

import alarmclock.logiclayer.manager.voicerecognition.SpeechEntryManager;
import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;

import java.io.IOException;

/**
 * Thread to manage the voice input
 */
public class RecogniserThread extends Thread {
  /**
   * The recognizer that is used to get the results from a microphone
   */
  private LiveSpeechRecognizer recognizer;
  private final SpeechEntryManager speechEntryManager = SpeechEntryManager.getInstance();

  /**
   * Constructor used to initialize the <code>recognizer</code> with the given <code>configuration</code>
   *
   * @param configuration the {@link Configuration} needed for the recognizer
   */
  public RecogniserThread(Configuration configuration) {
    initSpeechRecognizer(configuration);
  }

  @Override
  public void run() {
    while (true) {
      addResult();
    }
  }

  private void addResult() {
    try {
      speechEntryManager.addResult(getResult());
    } catch (IllegalStateException ignored) {
    }
  }

  private SpeechResult getResult() {
    SpeechResult result;
    do {
      result = recognizer.getResult();
    } while (result == null);
    return result;
  }

  private void initSpeechRecognizer(Configuration configuration) {
    try {
      recognizer = new LiveSpeechRecognizer(configuration);
    } catch (IllegalArgumentException e) {
      System.out.println("No Microphone connected!");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * starts the recognizer
   *
   * @return true if the starting the recognizer was successful, false otherwise
   */
  public boolean startRecognition() {
    if (recognizer != null) {
      recognizer.startRecognition(true);
      return true;
    }
    return false;
  }

  /**
   * stops the recognizer
   */
  public void stopRecognition() {
    recognizer.stopRecognition();
  }
}