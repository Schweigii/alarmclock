package alarmclock.logiclayer.manager.voicerecognition.threadmanagement.threads;

import alarmclock.logiclayer.manager.voicerecognition.SpeechEntryManager;
import alarmclock.logiclayer.manager.voicerecognition.voiceaction.SpeechActionEnum;
import edu.cmu.sphinx.api.SpeechResult;
import alarmclock.logiclayer.manager.voicerecognition.voiceaction.VocalInput;

import java.util.Optional;

/**
 * Thread to operate on voice input
 */
public class RecognitionThread extends Thread {
  /**
   * The manager object that holds all the speech result of the {@link RecogniserThread}
   */
  private SpeechEntryManager entryManager = SpeechEntryManager.getInstance();

  @Override
  public void run() {
    while (true) {
      getResult();
    }
  }

  private void getResult() {
    Optional<SpeechResult> result;
    if ((result = entryManager.getResult()).isPresent()) {
      String hypothesis = result.get().getHypothesis();
      SpeechActionEnum.activate(new VocalInput(hypothesis));
    }
  }
}