package alarmclock.logiclayer.manager.voicerecognition.exceptions;

import java.time.LocalDateTime;

/**
 * This Exception defines a {@link LocalDateTime} that is not valid to be set as an alarm
 */
public class InvalidAlarmDateTimeException extends VocalException {
  /**
   * The date time that is invalid
   */
  private LocalDateTime invalidAlarmDateTime;

  public InvalidAlarmDateTimeException(LocalDateTime invalidAlarmDateTime) {
    this.invalidAlarmDateTime = invalidAlarmDateTime;
  }

  @Override
  public String getMessage() {
    return "Alarm time or date is not valid" + (invalidAlarmDateTime != null ? ": + " + invalidAlarmDateTime.toString() : "");
  }

  @Override
  public String getSpeakableMessage() {
    return "I am sorry. The alarm you want to set is not valid";
  }
}
