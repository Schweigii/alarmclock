package alarmclock.logiclayer.manager.voicerecognition.exceptions;

import alarmclock.logiclayer.manager.texttospeech.SpeechManager;

/**
 * Abstract class that defines an Exception which message can be spoken via the TTS-Engine
 */
public abstract class VocalException extends Exception {
  /**
   * the speech manager used to speak the exception message
   */
  private final SpeechManager speechManager = SpeechManager.getInstance();

  /**
   * Defines the message of the exception
   *
   * @return the message
   */
  protected abstract String getSpeakableMessage();

  /**
   * uses the <code>speechManager</code> to speak out the defined message
   */
  public void speakException() {
    speechManager.speakMessage(getSpeakableMessage());
  }
}
