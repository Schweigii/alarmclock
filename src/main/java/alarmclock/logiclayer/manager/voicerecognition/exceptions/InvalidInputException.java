package alarmclock.logiclayer.manager.voicerecognition.exceptions;

/**
 * This Exception is used for voice input
 * Is thrown when a voice input is invalid
 */
public class InvalidInputException extends VocalException {

  @Override
  public String getSpeakableMessage() {
    return "I could not recognize your input. Please try again";
  }
}
