package alarmclock.logiclayer.manager.obersvers;

/**
 * This interface has to be implemented by every class that has to be updated on any
 * change on an alarm
 */
public interface AlarmObserver {
  void update();
}
