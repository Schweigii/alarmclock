package alarmclock.logiclayer.manager.alarmmanager;

import alarmclock.logiclayer.manager.obersvers.AlarmObserver;
import alarmclock.logiclayer.manager.texttospeech.SpeechManager;
import alarmclock.logiclayer.manager.voicerecognition.exceptions.InvalidAlarmDateTimeException;
import alarmclock.datalayer.valueobjectsconnectors.alarm.AlarmConnector;
import alarmclock.datalayer.valueobjectsconnectors.alarm.AlarmConnectorFactory;
import alarmclock.datalayer.valueobjects.alarm.Alarms;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.*;

/**
 * A singleton Manager that holds an {@link Alarms} object containing all the set alarms
 * <p>
 * to retrieve updates when alarms get changed, implements the {@link AlarmObserver} interface and use the
 * {@link AlarmManager#addObserver(AlarmObserver)} to add the class to the list of observers
 */
public class AlarmManager {
  /**
   * the single {@link Alarms} object handled by this manager
   */
  private Alarms alarms;
  /**
   * the {@link List} of {@link AlarmObserver} that will be notified when an alarm gets changed
   */
  private List<AlarmObserver> alarmObservers = new ArrayList<>();
  /**
   * The connector for the alarms value object
   */
  private AlarmConnector alarmConnector;
  /**
   * The speech manager for TTS
   */
  private SpeechManager speechManager;
  /**
   * The clock used for the local date time
   */
  private Clock clock;

  /**
   * this constructor is only called once by the {@link InstanceHolder}
   * reads the current alarms from an existing file or creates a new alarm object if no file exists
   */
  AlarmManager(AlarmConnector alarmConnector, SpeechManager speechManager, Clock clock) {
    this.alarmConnector = alarmConnector;
    this.speechManager = speechManager;
    this.clock = clock;
    alarms = alarmConnector.readValueObject().orElse(new Alarms());
    updateAlarm();
  }

  private static class InstanceHolder {
    public static final AlarmManager INSTANCE = new AlarmManager(AlarmConnectorFactory.getInstance(), SpeechManager.getInstance(), Clock.systemDefaultZone());
  }

  /**
   * @return the single existing {@link AlarmManager} instance
   */
  public static AlarmManager getInstance() {
    return InstanceHolder.INSTANCE;
  }

  /**
   * adds an {@link AlarmObserver} to the list of observers
   *
   * @param alarmObserver the alarm observer object to add
   */
  public void addObserver(AlarmObserver alarmObserver) {
    this.alarmObservers.add(alarmObserver);
  }

  /**
   * removes an {@link AlarmObserver} from the list of observers
   *
   * @param alarmObserver the alarm observer object to remove
   */
  public void removeObserver(AlarmObserver alarmObserver) {
    this.alarmObservers.remove(alarmObserver);
  }

  /**
   * gets the one alarm date time from the <code>alarms</code> object, that is the nearest to {@link LocalDateTime#now()}
   *
   * @return an {@link Optional} of type {@link LocalDateTime}
   */
  public Optional<LocalDateTime> getNextAlarm() {
    Iterator<LocalDateTime> iterator = alarms.getAlarmDateTimes().iterator();
    return iterator.hasNext() ? Optional.of(iterator.next()) : Optional.empty();
  }

  /**
   * retrieves all the alarm date times from the <code>alarms</code> object
   *
   * @return a {@link TreeSet} of type {@link LocalDateTime}
   */
  public Set<LocalDateTime> getAllAlarmDateTimes() {
    return alarms.getAlarmDateTimes();
  }

  /**
   * sets a new alarm date time in the <code>alarms</code> object
   * if the <code>alarmDateTime</code> parameter is after the {@link LocalDateTime#now()}
   *
   * @param alarmDateTime the alarm date time to set
   * @throws InvalidAlarmDateTimeException if the alarm date time is before or equal to {@link LocalDateTime#now()}
   */
  public void setNewAlarmTime(LocalDateTime alarmDateTime) throws InvalidAlarmDateTimeException {
    if (alarmDateTime.isAfter(LocalDateTime.now(clock))) {
      alarms.addAlarmDateTime(alarmDateTime);
      updateAlarm();
    } else {
      throw new InvalidAlarmDateTimeException(alarmDateTime);
    }
  }

  /**
   * deletes an alarm from the <code>alarms</code> object
   *
   * @param alarmDateTime the alarm date time to delete
   */
  public void deleteAlarmTime(LocalDateTime alarmDateTime) {
    alarms.getAlarmDateTimes().remove(alarmDateTime);
    updateAlarm();
  }

  /**
   * checks all the alarm date times if any are before {@link LocalDateTime#now()}
   *
   * @return true if any alarm date times are before {@link LocalDateTime#now()}; false if not
   */
  public boolean checkAlarm() {
    if (alarms.getAlarmDateTimes().stream().anyMatch(localDateTime -> localDateTime.isBefore(LocalDateTime.now(clock)))) {
      updateAlarm();
      return true;
    }
    return false;
  }

  /**
   * This method is called internally to update the date times of the alarms object and remove outdated date times
   * all observers are notified on update completion
   */
  private void updateAlarm() {
    Set<LocalDateTime> alarmDateTimes = alarms.getAlarmDateTimes();
    alarmDateTimes.removeIf(localDateTime -> localDateTime.isBefore(LocalDateTime.now(clock)));
    alarmConnector.writeValueObject(alarms);
    notifyObservers();
  }

  /**
   * Uses the TTS Engine to speak out the current date and time
   */
  public void triggerAlarm() {
    speechManager.speakAlarm(LocalDateTime.now(clock));
  }

  /**
   * this method is only called from the {@link AlarmManager#updateAlarm()} to notify all observers that the alarms has changed
   */
  private void notifyObservers() {
    alarmObservers.forEach(AlarmObserver::update);
  }

}
