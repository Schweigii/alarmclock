package alarmclock.logiclayer.manager.texttospeech;

import java.time.LocalDate;
import java.time.format.DateTimeFormatterBuilder;
import java.util.HashMap;
import java.util.Map;

import static java.time.temporal.ChronoField.DAY_OF_MONTH;

/**
 * This class is used to format a {@link LocalDate} object into a form that can be used in the TTS-Engine
 */
class LocalDateFormatter {
  /**
   * a map which keys represent days of a month, and which values represent suffix of the day
   * e.g. Key = 1, Value = First
   */
  private Map<Long, String> suffixMap = new HashMap<>();
  /**
   * the builder used to format the date
   */
  private DateTimeFormatterBuilder builder;

  /**
   * this constructor inits the <code>suffixMap</code>
   */
  LocalDateFormatter() {
    initSuffixMap();
  }

  /**
   * initializes the builder
   *
   * @return this
   */
  public LocalDateFormatter initBuilder() {
    builder = new DateTimeFormatterBuilder();
    return this;
  }

  /**
   * attaches the word "on" to the builder
   *
   * @return this
   */
  public LocalDateFormatter useOn() {
    builder.appendPattern(" 'on' ");
    return this;
  }

  /**
   * attaches the word "the" to the builder
   *
   * @return this
   */
  public LocalDateFormatter useThe() {
    builder.appendPattern(" 'the' ");
    return this;
  }

  /**
   * attaches a "," to the builder
   *
   * @return this
   */
  public LocalDateFormatter useComma() {
    builder.appendPattern(" ',' ");
    return this;
  }

  /**
   * attaches the word "of" to the builder
   *
   * @return this
   */
  public LocalDateFormatter useOf() {
    builder.appendPattern(" 'of' ");
    return this;
  }

  /**
   * attaches the day name to the builder
   *
   * @param localDate the date to get the day name from
   * @return this
   */
  public LocalDateFormatter useDayOfWeekName(LocalDate localDate) {
    builder.appendPattern(" '" + localDate.getDayOfWeek().name() + "' ");
    return this;
  }

  /**
   * appends the formatted day of the month to the builder
   * e.g. "4TH"
   *
   * @return this
   */
  public LocalDateFormatter useDayOfMonthSuffixMap() {
    builder.appendText(DAY_OF_MONTH, suffixMap);
    return this;
  }

  /**
   * uses the month of format 'MMMM'
   * e.g. 'January' or 'March'
   *
   * @return this
   */
  public LocalDateFormatter useMonth() {
    builder.appendPattern("MMMM");
    return this;
  }

  /**
   * uses the year of format 'yyyy'
   * e.g. '2020'
   *
   * @return this
   */
  public LocalDateFormatter useYear() {
    builder.appendPattern("yyyy");
    return this;
  }

  /**
   * builds a default string out of a {@link LocalDate} for the TTS-Engine
   * String looks like:
   * "on Friday, the 26th of March, 2020"
   *
   * @param localDate the local date to be parsed
   * @return the default string
   */
  public String buildDefaultDateString(LocalDate localDate) {
    return initBuilder()
        .useOn()
        .useDayOfWeekName(localDate)
        .useComma()
        .useThe()
        .useDayOfMonthSuffixMap()
        .useOf()
        .useMonth()
        .useComma()
        .useYear()
        .build(localDate);
  }

  /**
   * builds a formatted string from a {@link LocalDate}
   *
   * @param localDate the local date to format
   * @return the formatted string
   */
  public String build(LocalDate localDate) {
    return builder.toFormatter().format(localDate);
  }

  private void initSuffixMap() {
    for (long i = 1L; i <= 31L; i++) {
      suffixMap.put(i, DayOfMonthSuffix.getDayOfMonthSuffixForDayOfMonth((int) i));
    }
  }

  private enum DayOfMonthSuffix {
    ST,
    ND,
    RD,
    TH,
    ;

    public static String getDayOfMonthSuffixForDayOfMonth(int dayOfMonth) {
      switch (dayOfMonth) {
        case 1:
        case 21:
        case 31:
          return dayOfMonth + ST.name();
        case 2:
        case 22:
          return dayOfMonth + ND.name();
        case 3:
        case 23:
          return dayOfMonth + RD.name();
        default:
          return dayOfMonth + TH.name();
      }
    }
  }
}
