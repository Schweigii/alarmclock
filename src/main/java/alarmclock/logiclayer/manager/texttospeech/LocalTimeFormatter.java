package alarmclock.logiclayer.manager.texttospeech;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Locale;

/**
 * This class is used to format a {@link LocalTime} object into a form that can be used in the TTS-Engine
 */
class LocalTimeFormatter {
  LocalTimeFormatter() {
  }

  /**
   * formats a given {@link LocalTime} object into a usable form for the TTS-Engine
   * String looks like:
   * "12 17 pm" or "12 o'clock"
   *
   * @param localTime the local time to format
   * @return the formatted String
   */
  public String useFormattedTimeWithSuffix(LocalTime localTime) {
    String specialHour = SpecialHour.pronunciationForTime(localTime);
    DateTimeFormatterBuilder builder = new DateTimeFormatterBuilder();
    builder.appendPattern("'" + (specialHour != null ? specialHour : localTime.format(DateTimeFormatter.ofPattern("h:mm a", Locale.US))) + "'");
    return builder.toFormatter().format(localTime);
  }

  private enum SpecialMinute {
    O_CLOCK("o clock"),
    QUARTER_PAST("quarter past"),
    HALF_PAST("half past"),
    QUARTER_TO("quarter to"),
    ;

    private final String pronunciation;

    SpecialMinute(String pronunciation) {
      this.pronunciation = pronunciation;
    }

    public static String pronunciationForTime(LocalTime localTime) {
      switch (localTime.getMinute()) {
        case 0:
          return localTime.getHour() + " " + O_CLOCK.pronunciation;
        case 15:
          return checkForSpecialHour(localTime, QUARTER_PAST);
        case 30:
          return checkForSpecialHour(localTime, HALF_PAST);
        case 45:
          return checkForSpecialHour(localTime.plusHours(1), QUARTER_TO);
        default:
          return null;
      }

    }

    private static String checkForSpecialHour(LocalTime localTime, SpecialMinute specialMinute) {
      if (localTime.getHour() == 12) {
        return specialMinute.pronunciation + " " + SpecialHour.NOON;
      }
      if (localTime.getHour() == 0) {
        return specialMinute.pronunciation + " " + SpecialHour.MIDNIGHT;
      }
      return specialMinute.pronunciation + " " + localTime.format(DateTimeFormatter.ofPattern("h a", Locale.US));
    }
  }

  private enum SpecialHour {
    NOON,
    MIDNIGHT,
    ;

    public static String pronunciationForTime(LocalTime localTime) {
      if (localTime == LocalTime.NOON) {
        return NOON.name();
      }
      if (localTime == LocalTime.MIDNIGHT) {
        return MIDNIGHT.name();
      }
      return SpecialMinute.pronunciationForTime(localTime);
    }
  }
}
