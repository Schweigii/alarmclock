package alarmclock.logiclayer.manager.texttospeech;

import java.util.Arrays;

/**
 * This enum defines the greeting lines of an alarm voice output
 */
enum Greetings {
  MORNING("Good morning, sir!", 0, 10),
  NOON("Good day, sir!", 11, 13),
  AFTERNOON("Good afternoon, sir!", 14, 17),
  EVENING("Good evening, sir!", 18, 20),
  NIGHT("Good night, sir!", 21, 23),
  DEFAULT("Hello, sir!", 999, 999),
  ;

  private final String greeting;
  private final int hourOfDayFrom;
  private final int hourOfDayUntil;

  Greetings(String greeting, int hourOfDayFrom, int hourOfDayUntil) {
    this.greeting = greeting;
    this.hourOfDayFrom = hourOfDayFrom;
    this.hourOfDayUntil = hourOfDayUntil;
  }

  public String getGreeting() {
    return greeting;
  }

  /**
   * parses the given <code>hour</code> to a value of this enum
   *
   * @param hour the hour to be parsed
   * @return the Greeting value of the given hour, {@link Greetings#DEFAULT} if the hour is invalid
   */
  public static Greetings getGreetingForHour(int hour) {
    return Arrays.stream(values())
        .filter(greetings -> greetings.hourOfDayFrom <= hour && greetings.hourOfDayUntil >= hour)
        .findFirst()
        .orElse(DEFAULT);
  }
}
