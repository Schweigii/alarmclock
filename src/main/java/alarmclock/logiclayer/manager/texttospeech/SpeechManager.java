package alarmclock.logiclayer.manager.texttospeech;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * This class manages connection to the TTS-Engine
 * its methods can be used to output Strings or objects to speakers
 */
public class SpeechManager {
  /**
   * The voice that is used to output messages to speakers
   */
  private final Voice voice;
  /**
   * the formatter for the local date formatting
   */
  private final LocalDateFormatter localDateFormatter;
  /**
   * the formatter for the local time formatting
   */
  private final LocalTimeFormatter localTimeFormatter;

  /**
   * This constructor initializes all attributes of this class, sets the System Property for the 'Kevin' voice to be used
   * and allocates the voice
   *
   * @param localDateFormatter the local date formatter object
   * @param localTimeFormatter the local time formatter object
   * @param voiceManager       the voice manager to get the voice from
   */
  SpeechManager(LocalDateFormatter localDateFormatter, LocalTimeFormatter localTimeFormatter, VoiceManager voiceManager) {
    System.setProperty("freetts.voices", "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");
    this.localDateFormatter = localDateFormatter;
    this.localTimeFormatter = localTimeFormatter;
    this.voice = voiceManager.getVoice("kevin16");
    this.voice.allocate();
    this.voice.setRate(150);
    this.voice.setPitch(100);
    this.voice.setVolume(20);
  }

  private static class InstanceHolder {
    public static final SpeechManager INSTANCE = new SpeechManager(new LocalDateFormatter(), new LocalTimeFormatter(), VoiceManager.getInstance());
  }

  public static SpeechManager getInstance() {
    return InstanceHolder.INSTANCE;
  }

  /**
   * speaks any string given to the output
   *
   * @param message the string to output
   */
  public void speakMessage(String message) {
    voice.speak(message);
  }

  /**
   * speaks the next alarm with a given {@link LocalDateTime} object
   *
   * @param localDateTime the local date time of the alarm
   */
  public void speakNextAlarm(LocalDateTime localDateTime) {
    voice.speak("The next alarm is at " + localTimeFormatter.useFormattedTimeWithSuffix(localDateTime.toLocalTime()));
    voice.speak(localDateFormatter.buildDefaultDateString(localDateTime.toLocalDate()));
  }

  /**
   * speaks that an alarm has been set on the given {@link LocalDateTime}
   *
   * @param localDateTime the local date time the alarm has been set on
   */
  public void speakAlarmSet(LocalDateTime localDateTime) {
    voice.speak("I set your alarm " + localDateFormatter.buildDefaultDateString(localDateTime.toLocalDate()));
    voice.speak(" at " + localTimeFormatter.useFormattedTimeWithSuffix(localDateTime.toLocalTime()));
  }

  /**
   * speaks an alarm date and time
   *
   * @param localDateTime the local date time of the alarm
   */
  public void speakAlarm(LocalDateTime localDateTime) {
    float volume = voice.getVolume();
    voice.setVolume(60);
    voice.speak(Greetings.getGreetingForHour(localDateTime.getHour()).getGreeting());
    voice.speak("It is currently " + localTimeFormatter.useFormattedTimeWithSuffix(localDateTime.toLocalTime()));
    voice.speak(localDateFormatter.buildDefaultDateString(localDateTime.toLocalDate()));
    voice.setVolume(volume);
  }

  /**
   * speaks the current date and time
   */
  public void speakTime() {
    voice.speak("It is currently " + localTimeFormatter.useFormattedTimeWithSuffix(LocalTime.now()));
    voice.speak(localDateFormatter.buildDefaultDateString(LocalDate.now()));
  }
}
