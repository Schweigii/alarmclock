package alarmclock.datalayer.valueobjects.alarm;

import alarmclock.datalayer.valueobjectsconnectors.alarm.AlarmConnector;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.TreeSet;

/**
 * the value object that holds the {@link LocalDateTime} which represent the alarm dates and times
 *
 * @see AlarmConnector for Serialization / Deserialization handling
 */
public class Alarms implements Serializable {
  /**
   * the Set of {@link LocalDateTime} containing the alarm dates and times
   */
  private Set<LocalDateTime> alarmDateTimes = new TreeSet<>();

  /**
   * @return the <code>alarmDateTimes</code> attribute
   */
  public Set<LocalDateTime> getAlarmDateTimes() {
    return alarmDateTimes instanceof TreeSet ? alarmDateTimes : (alarmDateTimes = new TreeSet<>(alarmDateTimes));
  }

  /**
   * adds an alarm to existing ones, without checking the newly set time
   *
   * @param alarmDateTime the alarm date and time to add
   */
  public void addAlarmDateTime(LocalDateTime alarmDateTime) {
    alarmDateTimes.add(alarmDateTime);
  }
}
