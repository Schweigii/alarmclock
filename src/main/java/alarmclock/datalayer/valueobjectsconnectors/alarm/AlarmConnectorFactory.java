package alarmclock.datalayer.valueobjectsconnectors.alarm;

/**
 * A connector factory that holds a single instance of {@link AlarmConnector}
 */
public final class AlarmConnectorFactory {
  private static final String FILENAME = "Alarm.json";
  private static final String FILEPATH = "src/main/java/alarmclock/datalayer/data/";

  private static final class InstanceHolder {
    public static AlarmConnector INSTANCE = new AlarmConnector(FILEPATH + FILENAME);
  }

  /**
   * @return the single {@link AlarmConnector} instance of this Factory
   */
  public static AlarmConnector getInstance() {
    return InstanceHolder.INSTANCE;
  }
}
