package alarmclock.datalayer.valueobjectsconnectors.alarm;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;
import alarmclock.datalayer.valueobjectsconnectors.ValueObjectConnector;
import alarmclock.datalayer.valueobjects.alarm.Alarms;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;

/**
 * the connector class that handles reading and writing of the {@link Alarms} value object
 */
public class AlarmConnector extends ValueObjectConnector<Alarms> {
  /**
   * the file name used for reading and writing the value object
   */
  private String fileName;

  /**
   * creates an instance of this class with a {@link GsonBuilder} for serializing and deserializing the {@link LocalDateTime}
   */
  AlarmConnector(String fileName) {
    super(new GsonBuilder()
        .registerTypeAdapter(LocalDateTime.class, (JsonDeserializer<LocalDateTime>) (json, typeOfT, context) -> {
          Instant instant = Instant.ofEpochMilli(json.getAsJsonPrimitive().getAsLong());
          return LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        })
        .registerTypeAdapter(LocalDateTime.class, (JsonSerializer<LocalDateTime>) (src, typeOfSrc, context) ->
            new JsonPrimitive(src.toInstant(ZoneId.systemDefault().getRules().getOffset(src)).toEpochMilli())));
    this.fileName = fileName;
  }

  @Override
  public void writeValueObject(Alarms valueObject) {
    try {
      toJson(valueObject, fileName);
    } catch (IOException e) {
      System.out.println("an error occurred while write to " + fileName);
      e.printStackTrace();
    }
  }

  @Override
  public Optional<Alarms> readValueObject() {
    try {
      return Optional.ofNullable(fromJson(fileName, Alarms.class));
    } catch (IOException e) {
      System.out.println("failed to read " + fileName);
      return Optional.empty();
    }
  }
}
