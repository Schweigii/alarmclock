package alarmclock.datalayer.valueobjectsconnectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

/**
 * A generic abstract connector class that allows value objects that implements {@link Serializable}
 * to write the objects data to a JSON-File and read the same File and parse the read data to the object
 *
 * @param <T> The type of object used to read / write the JSON File
 */
public abstract class ValueObjectConnector<T extends Serializable> {
  /**
   * the {@link Gson} object used to write / read the JSON-File
   */
  private Gson gson;

  /**
   * Initializes the {@link Gson} attribute of this class with its standard Constructor
   */
  protected ValueObjectConnector() {
    this.gson = new Gson();
  }

  /**
   * Initializes the {@link Gson} attribute of this class using a {@link GsonBuilder} object
   * to allow for customized deserialization
   * e.g. for LocalDateTime that is not supported by Gson
   *
   * @param gsonBuilder the {@link GsonBuilder} object used to create the {@link Gson} object
   */
  protected ValueObjectConnector(GsonBuilder gsonBuilder) {
    this.gson = gsonBuilder.create();
  }

  /**
   * Writes the attributes of <code>valueObject</code> of type {@link T} to a file with name <code>fileName</code>
   * Automatically creates a new File if the file with the name of <code>fileName</code> is not found.
   * Overrides all data in an existing file with the data of the given value object
   *
   * @param valueObject the value object to write to the JSON file
   * @param fileName    the file name
   * @throws IOException if an Exception occurs during writing the file
   */
  protected final void toJson(T valueObject, String fileName) throws IOException {
    FileWriter writer = new FileWriter(fileName);
    gson.toJson(valueObject, writer);
    writer.flush();
    writer.close();
  }

  /**
   * Parses the JSON data from a given <code>fileName</code> to create an Object of class {@link T}
   * and returns said object
   *
   * @param fileName the file name
   * @param clazz    the class as reference to parse the JSON data
   * @return the parsed Object of type {@link T}
   * @throws IOException if an Exception occurs during reading from the file
   */
  protected final T fromJson(String fileName, Class<T> clazz) throws IOException {
    FileReader fileReader = new FileReader(fileName);
    T result = gson.fromJson(fileReader, clazz);
    fileReader.close();
    return result;
  }

  /**
   * abstract method to handle the file writing of value object class of the connector
   *
   * @param valueObject the value object to write in the file
   */
  public abstract void writeValueObject(T valueObject);

  /**
   * abstract method to handle the file reading for the value object of the connector
   *
   * @return an {@link Optional} of type {@link T}
   */
  public abstract Optional<T> readValueObject();
}
