package alarmclock.datalayer.valueobjects.alarm;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.*;

public class AlarmsTest {
  private Alarms alarms;

  @Before
  public void setUp() {
    alarms = new Alarms();
  }

  @Test
  public void alarmDateTimesInteractions() {
    LocalDateTime alarmDateTime = LocalDateTime.of(2020, 10, 10, 10, 10, 10);

    Set<LocalDateTime> alarmDateTimes = alarms.getAlarmDateTimes();

    assertTrue(alarmDateTimes.isEmpty());
    assertTrue(alarmDateTimes instanceof TreeSet);

    alarms.addAlarmDateTime(alarmDateTime);

    alarmDateTimes = alarms.getAlarmDateTimes();

    assertFalse(alarmDateTimes.isEmpty());
    assertTrue(alarmDateTimes instanceof TreeSet);
    assertEquals(1, alarmDateTimes.size());
    assertEquals(alarmDateTime, alarmDateTimes.iterator().next());
  }
}