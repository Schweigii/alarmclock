package alarmclock.datalayer.valueobjectsconnectors.alarm;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AlarmConnectorFactoryTest {

  @Test
  public void getInstance() {
    AlarmConnector connectorOne = AlarmConnectorFactory.getInstance();
    AlarmConnector connectorTwo = AlarmConnectorFactory.getInstance();
    assertEquals(connectorOne, connectorTwo);
  }
}