package alarmclock.datalayer.valueobjectsconnectors.alarm;

import alarmclock.datalayer.valueobjects.alarm.Alarms;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AlarmConnectorTest {
  private AlarmConnector connector;

  @Before
  public void setUp() {
    connector = new AlarmConnector("AlarmTest.json");
  }

  @After
  public void tearDown() {
    File file = new File("AlarmTest.json");
    file.delete();
  }

  @Test
  public void writeReadValueObject() {
    Alarms alarms = new Alarms();
    LocalDateTime testDateTime = LocalDateTime.of(2020, 10, 4, 10, 10);
    alarms.addAlarmDateTime(testDateTime);
    connector.writeValueObject(alarms);
    Optional<Alarms> result = connector.readValueObject();
    assertTrue(result.isPresent());
    assertEquals(alarms.getAlarmDateTimes(), result.get().getAlarmDateTimes());
  }

  @Test
  public void writeReadEmptyObject() {
    Alarms alarms = new Alarms();
    connector.writeValueObject(alarms);
    Optional<Alarms> result = connector.readValueObject();
    assertTrue(result.isPresent());
    assertTrue(result.get().getAlarmDateTimes().isEmpty());
  }
}