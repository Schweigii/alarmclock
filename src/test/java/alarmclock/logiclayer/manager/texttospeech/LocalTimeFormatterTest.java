package alarmclock.logiclayer.manager.texttospeech;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalTime;

import static org.junit.Assert.assertEquals;

public class LocalTimeFormatterTest {
  private LocalTimeFormatter localTimeFormatter;

  @Before
  public void setUp() {
    localTimeFormatter = new LocalTimeFormatter();
  }

  @Test
  public void useFormattedTimeWithSuffixQuarterPast() {
    LocalTime localTime = LocalTime.of(10, 15, 12);
    String expectedTime = "quarter past 10 AM";
    String formattedTime = localTimeFormatter.useFormattedTimeWithSuffix(localTime);

    assertEquals(expectedTime, formattedTime);
  }

  @Test
  public void useFormattedTimeWithSuffixNoon() {
    LocalTime localTime = LocalTime.of(12, 0, 0);
    String expectedTime = "NOON";
    String formattedTime = localTimeFormatter.useFormattedTimeWithSuffix(localTime);

    assertEquals(expectedTime, formattedTime);
  }
}