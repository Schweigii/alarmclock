package alarmclock.logiclayer.manager.texttospeech;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class LocalDateFormatterTest {
  private LocalDateFormatter localDateFormatter;
  private LocalDate localDate;

  @Before
  public void setUp() {
    localDateFormatter = new LocalDateFormatter();
    localDate = LocalDate.of(2020, 10, 4);
  }

  @Test
  public void useOn() {
    String onString = localDateFormatter.initBuilder().useOn().build(localDate);

    assertEquals(" on ", onString);
  }

  @Test
  public void useThe() {
    String theString = localDateFormatter.initBuilder().useThe().build(localDate);

    assertEquals(" the ", theString);
  }

  @Test
  public void useComma() {
    String commaString = localDateFormatter.initBuilder().useComma().build(localDate);

    assertEquals(" , ", commaString);
  }

  @Test
  public void useOf() {
    String ofString = localDateFormatter.initBuilder().useOf().build(localDate);

    assertEquals(" of ", ofString);
  }

  @Test
  public void useDayOfWeekName() {
    String dayOfWeekString = localDateFormatter.initBuilder().useDayOfWeekName(localDate).build(localDate);

    assertEquals(" SUNDAY ", dayOfWeekString);
  }

  @Test
  public void useDayOfMonthSuffixMap() {
    String dayOfMonthString = localDateFormatter.initBuilder().useDayOfMonthSuffixMap().build(localDate);

    assertEquals("4TH", dayOfMonthString);
  }

  @Test
  public void useMonth() {
    String monthString = localDateFormatter.initBuilder().useMonth().build(localDate);

    assertEquals("Oktober", monthString);
  }

  @Test
  public void useYear() {
    String yearString = localDateFormatter.initBuilder().useYear().build(localDate);

    assertEquals("2020", yearString);
  }

  @Test
  public void buildDefaultDateString() {
    String defaultDateString = localDateFormatter.buildDefaultDateString(localDate);

    assertEquals(" on  SUNDAY  ,  the 4TH of Oktober , 2020", defaultDateString);
  }
}