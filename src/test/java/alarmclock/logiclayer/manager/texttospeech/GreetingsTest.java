package alarmclock.logiclayer.manager.texttospeech;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GreetingsTest {

  @Test
  public void getGreetingForHourMorning() {
    Greetings greeting = Greetings.getGreetingForHour(5);

    assertEquals(Greetings.MORNING, greeting);
  }

  @Test
  public void getGreetingForHourDefault() {
    Greetings greeting = Greetings.getGreetingForHour(63);

    assertEquals(Greetings.DEFAULT, greeting);
  }
}