package alarmclock.logiclayer.manager.alarmmanager;

import alarmclock.datalayer.valueobjects.alarm.Alarms;
import alarmclock.datalayer.valueobjectsconnectors.alarm.AlarmConnector;
import alarmclock.logiclayer.manager.texttospeech.SpeechManager;
import alarmclock.logiclayer.manager.voicerecognition.exceptions.InvalidAlarmDateTimeException;
import org.junit.Before;
import org.junit.Test;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class AlarmManagerTest {
  private AlarmManager alarmManager;
  private AlarmConnector alarmConnector;
  private SpeechManager speechManager;
  private Clock clock;
  private static final LocalDateTime NOW = LocalDateTime.of(2020, 9, 9, 10, 10, 10);
  private static final LocalDateTime newAlarmDateTime = LocalDateTime.of(2020, 10, 10, 10, 10, 10);
  private static final LocalDateTime nextAlarmDateTime = LocalDateTime.of(2020, 10, 9, 10, 10, 10);

  @Before
  public void setUp() {
    alarmConnector = mock(AlarmConnector.class);
    speechManager = mock(SpeechManager.class);
    clock = Clock.fixed(NOW.toInstant(ZoneOffset.UTC), ZoneId.systemDefault());
    Alarms alarms = new Alarms();
    alarms.addAlarmDateTime(newAlarmDateTime);
    alarms.addAlarmDateTime(nextAlarmDateTime);

    when(alarmConnector.readValueObject()).thenReturn(Optional.of(alarms));
    alarmManager = new AlarmManager(alarmConnector, speechManager, clock);
  }

  @Test
  public void getNextAlarm() {
    Optional<LocalDateTime> realNextAlarmDateTime = alarmManager.getNextAlarm();

    assertTrue(realNextAlarmDateTime.isPresent());

    LocalDateTime realDateTime = realNextAlarmDateTime.get();

    assertEquals(nextAlarmDateTime, realDateTime);
  }

  @Test
  public void getAllAlarmDateTimes() {
    Set<LocalDateTime> localDateTimes = alarmManager.getAllAlarmDateTimes();

    assertNotNull(localDateTimes);
    assertTrue(localDateTimes instanceof TreeSet);
    assertFalse(localDateTimes.isEmpty());
    assertEquals(2, localDateTimes.size());

    Iterator<LocalDateTime> iterator = localDateTimes.iterator();

    assertEquals(nextAlarmDateTime, iterator.next());
    assertEquals(newAlarmDateTime, iterator.next());
  }

  @Test
  public void setNewAlarmTime() throws InvalidAlarmDateTimeException {
    LocalDateTime newestAlarm = LocalDateTime.of(2020, 11, 11, 11, 11, 11);
    alarmManager.setNewAlarmTime(newestAlarm);

    Set<LocalDateTime> localDateTimes = alarmManager.getAllAlarmDateTimes();

    assertNotNull(localDateTimes);
    assertTrue(localDateTimes instanceof TreeSet);
    assertFalse(localDateTimes.isEmpty());
    assertEquals(3, localDateTimes.size());

    Iterator<LocalDateTime> iterator = localDateTimes.iterator();

    assertEquals(nextAlarmDateTime, iterator.next());
    assertEquals(newAlarmDateTime, iterator.next());
    assertEquals(newestAlarm, iterator.next());
  }

  @Test
  public void checkAlarm() {
    LocalDateTime newestLocalDateTime = LocalDateTime.of(2020, 9, 9, 10, 0, 0);
    alarmManager.getAllAlarmDateTimes().add(newestLocalDateTime);

    assertTrue(alarmManager.checkAlarm());
    Set<LocalDateTime> localDateTimes = alarmManager.getAllAlarmDateTimes();

    assertNotNull(localDateTimes);
    assertTrue(localDateTimes instanceof TreeSet);
    assertFalse(localDateTimes.isEmpty());
    assertEquals(2, localDateTimes.size());

    Iterator<LocalDateTime> iterator = localDateTimes.iterator();

    assertEquals(nextAlarmDateTime, iterator.next());
    assertEquals(newAlarmDateTime, iterator.next());
  }

  @Test
  public void soundAlarm() {
    alarmManager.triggerAlarm();

    verify(speechManager, times(1)).speakAlarm(LocalDateTime.now(clock));
  }
}