package alarmclock.logiclayer.manager.voicerecognition.inputparser;

import alarmclock.logiclayer.manager.voicerecognition.exceptions.InvalidInputException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SpeechMeridianTest {

  @Test
  public void getMeridianForHypothesis() throws InvalidInputException {
    String hypothesis = "BEFORE MIDDAY";

    SpeechMeridian beforeMidday = SpeechMeridian.getMeridianForHypothesis(hypothesis);
    assertEquals(SpeechMeridian.AM, beforeMidday);
  }

  @Test(expected = InvalidInputException.class)
  public void getMeridianForHypothesisThrowingException() throws InvalidInputException {
    String hypothesis = "MIDDAY";

    SpeechMeridian.getMeridianForHypothesis(hypothesis);
  }
}