package alarmclock.logiclayer.manager.voicerecognition.inputparser;

import alarmclock.logiclayer.manager.voicerecognition.exceptions.InvalidInputException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SpeechDateTest {

  @Test(expected = InvalidInputException.class)
  public void getSpeechDateForHypothesisThrowingException() throws InvalidInputException {
    String hypothesis = "YESTERDAY";

    SpeechDate date = SpeechDate.getSpeechDateForHypothesis(hypothesis);
    assertEquals(SpeechDate.TOMORROW, date);
  }

  @Test
  public void getSpeechDateForHypothesis() throws InvalidInputException {
    String hypothesis = "TOMORROW";

    SpeechDate date = SpeechDate.getSpeechDateForHypothesis(hypothesis);
    assertEquals(SpeechDate.TOMORROW, date);
  }
}