package alarmclock.logiclayer.manager.voicerecognition.inputparser;

import alarmclock.logiclayer.manager.voicerecognition.exceptions.InvalidInputException;
import org.junit.Before;
import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SpeechInputDateTimeParserTest {
  private SpeechInputDateTimeParser manager;

  @Before
  public void setUp() {
    manager = new SpeechInputDateTimeParser();
  }

  @Test
  public void parseDateTimeHypothesisAM() throws InvalidInputException {
    String hypothesis = "THREE THIRTY BEFORE MIDDAY NEXT FRIDAY";

    LocalDateTime finishedDateTime = manager.parseDateTimeHypothesis(hypothesis);
    assertNotNull(finishedDateTime);
    assertEquals(3, finishedDateTime.getHour());
    assertEquals(30, finishedDateTime.getMinute());
    assertEquals(DayOfWeek.FRIDAY, finishedDateTime.getDayOfWeek());
  }

  @Test
  public void parseDateTimeHypothesisPM() throws InvalidInputException {
    String hypothesis = "EIGHT FIFTY FOUR AFTER MIDDAY NEXT MONDAY";

    LocalDateTime finishedDateTime = manager.parseDateTimeHypothesis(hypothesis);
    assertNotNull(finishedDateTime);
    assertEquals(20, finishedDateTime.getHour());
    assertEquals(54, finishedDateTime.getMinute());
    assertEquals(DayOfWeek.MONDAY, finishedDateTime.getDayOfWeek());
  }

  @Test(expected = InvalidInputException.class)
  public void parseDateTimeHypothesis() throws InvalidInputException {
    String hypothesis = "THREE THIRTY MIDDAY NEXT FRIDAY";

    manager.parseDateTimeHypothesis(hypothesis);
  }
}