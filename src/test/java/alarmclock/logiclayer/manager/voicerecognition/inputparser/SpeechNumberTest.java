package alarmclock.logiclayer.manager.voicerecognition.inputparser;

import alarmclock.logiclayer.manager.voicerecognition.exceptions.InvalidInputException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SpeechNumberTest {

  @Test(expected = InvalidInputException.class)
  public void getNumberForHypothesisThrowingException() throws InvalidInputException {
    String hypothesis = "THIRTYFIVE";

    SpeechNumber.getNumberForHypothesis(hypothesis);
  }

  @Test
  public void getNumberForHypothesis() throws InvalidInputException {
    String hypothesis = "THREE";

    SpeechNumber number = SpeechNumber.getNumberForHypothesis(hypothesis);
    assertEquals(3, number.getNumber());
  }

  @Test
  public void getHourForHypothesis() throws InvalidInputException {
    String hypothesisValid = "FIVE";

    SpeechNumber number = SpeechNumber.getHourForHypothesis(hypothesisValid);
    assertEquals(5, number.getNumber());
  }

  @Test(expected = InvalidInputException.class)
  public void getHourForHypothesisThrowingException() throws InvalidInputException {
    String hypothesisValid = "THIRTEEN";

    SpeechNumber.getHourForHypothesis(hypothesisValid);
  }
}