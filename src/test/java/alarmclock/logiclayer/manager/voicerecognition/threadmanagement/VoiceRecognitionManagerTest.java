package alarmclock.logiclayer.manager.voicerecognition.threadmanagement;

import alarmclock.logiclayer.manager.voicerecognition.threadmanagement.threads.RecogniserThread;
import alarmclock.logiclayer.manager.voicerecognition.threadmanagement.threads.RecognitionThread;
import edu.cmu.sphinx.api.Configuration;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class VoiceRecognitionManagerTest {
  private VoiceRecognitionManager manager;
  private Configuration configuration;

  @Before
  public void setUp() {
    configuration = mock(Configuration.class);
    manager = new VoiceRecognitionManager(VoiceRecognitionManager.VoiceRecognitionConfiguration.CUSTOM, configuration);
  }

  @Test
  public void configurationInit() {
    verify(configuration, times(1)).setAcousticModelPath(anyString());
    verify(configuration, times(1)).setDictionaryPath(anyString());
    verify(configuration, times(1)).setLanguageModelPath(anyString());
  }

  @Test
  public void initRecogniserThread() {
    RecogniserThread thread = mock(RecogniserThread.class);
    manager.initRecogniserThread(thread);

    verify(thread, times(1)).startRecognition();
  }

  @Test
  public void initRecognitionThread() {
    RecognitionThread thread = mock(RecognitionThread.class);
    manager.initRecognitionThread(thread);

    assertTrue(thread.isDaemon());
    verify(thread, times(1)).start();
  }
}